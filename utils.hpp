#ifndef UTILS_HPP_INCLUDED
#define UTILS_HPP_INCLUDED

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <string>
#include <string_view>
#include <algorithm>
#include <filesystem>
#include <cmath>
#include <sstream>

inline static void str_lower(std::string& s)
{
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c){ return std::tolower(c); });
}

template <typename T>
bool get_element_name(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->Name();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}
template <typename T>
bool get_element_value(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->Value();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}
template <typename T>
bool get_element_text(const T *e, std::string& s) /// true on success
{
    assert(e != nullptr);
    auto res = e->GetText();
    if (res != nullptr) { s = res; return true; }
    s = "";
    return false;
}

inline static bool str_rem_newlines(std::string& s) /// true if modified
{
    std::size_t pos0 = 0;
    std::size_t pos1 = 0; // = str.find("\n");
    std::string res;
    bool rrr = false;
    while (true)
    {
        pos1 = s.find('\n',pos0);
        std::string str1 = s.substr(pos0, (pos1-pos0));
        if (!str1.empty())
        {
            if (!res.empty()) { res += '_'; rrr = true; }
            res += str1;
        }
        if (pos1 == std::string::npos) { break; }
        pos0 = pos1+1;
    }
    //const bool rrr = (s.compare(res) == 0);
    s = res;
    return rrr;
}

inline static bool str_compare(const char* s0, const char* s1) /// true on match, case-sensitive
{
    if ((s0 == nullptr) || (s1 == nullptr))
    {
        return (s0 == s1);
    }
    //size_t i =
    while (true)
    {
        if (*s0 != *s1) { return false; }
        if (*s0 == '\0') { return true; }
        ++s0; ++s1;
    }
}
inline static bool str_compare(const std::string& s0, const std::string& s1) /// true on match, case-sensitive
{
    if (s0.length() != s1.length()) { return false; }
    return (s0.compare(s1) == 0);
}

inline static bool str_compare_nocs(const char* s0, const char* s1) /// true on match, case-insensitive
{
    if ((s0 == nullptr) || (s1 == nullptr))
    {
        return (s0 == s1);
    }
    //size_t i =
    while (true)
    {
        if (*s0 != *s1)
        {
            if (std::tolower(*s0) != std::tolower(*s1)) { return false; }
            //return false;
        }
        if (*s0 == '\0') { return true; }

        ++s0; ++s1;
    }
}
inline static bool str_compare_nocs(const std::string& s0, const std::string& s1) /// true on match, case-insensitive
{
    if (s0.length() != s1.length()) { return false; }
    return str_compare_nocs(s0.c_str(), s1.c_str());
}

// ###################################################################
template<typename T = uint64_t>
struct fumber_t /// Fancy nUMBER ... because i didn't have a better name for this
{
    enum TYPE
    {
        T_DECIMAL,  //!< 123
        T_HEX,      //!< 0x0F2C
        T_BIN,      //!< #10010000
        T_BIN2,     //!< 0b10010000
        T_ERROR     //!< Som Ting Wong
    };
    TYPE type;
    size_t num_chars; // the 0x, #, and 0b are not counted
    T val;
    fumber_t() : type(T_ERROR), num_chars(1), val(0)
    {}

    bool good() const
    {
        // max num_chars worst case should be the number of bits
        return ((type >= T_DECIMAL) && (type < T_ERROR) && (num_chars <= (2*8*sizeof(T))));
    }

    bool set(const std::string& s)
    {
        type = T_ERROR;
        if (s.empty()) { return false; }
        val = 0;
        const size_t len = s.length();
        size_t i = 0;
        {
            // try decimal, if fail due to weird chars - try the other ways
            while (i < len)
            {
                char c = s[i];
                if ((c >= '0') && (c <= '9')) { val = 10*val+(c-'0'); }
                else { break; }
                ++i;
            }
            if (i == len)
            {
                num_chars = i;
                type = T_DECIMAL;
                return true;
            }
        }
        if ((s[0] == '#') && (len > 1)) // cmsis-svd kind of #binary
        {
            i = 1;
            while (i < len)
            {
                char c = s[i];
                if ((c == '0') || (c == '1')) { val = (val<<1) | (c-'0'); }
                else { return false; }
                ++i;
            }
            num_chars = len-1;
            type = T_BIN;
            return true;
        }
        else if ((s[0] == '0') && (len > 2))
        {
            i = 2;
            if (s[1] == 'b') // binary2
            {
                while (i < len)
                {
                    char c = s[i];
                    if ((c == '0') || (c == '1')) { val = (val<<1) | (c-'0'); }
                    else { return false; }
                    ++i;
                }
                num_chars = len-2;
                type = T_BIN2;
                return true;
            }
            else if (s[1] == 'x') // hex
            {
                while (i < len)
                {
                    char c = s[i];
                    if      ((c >= '0') && (c <= '9')) { c -= '0'; }
                    else if ((c >= 'A') && (c <= 'F')) { c = (10+c-'A'); }
                    else if ((c >= 'a') && (c <= 'f')) { c = (10+c-'a'); }
                    else { return false; }
                    val = (val<<4) | c;
                    ++i;
                }
                num_chars = len-2;
                type = T_HEX;
                return true;
            }
        }
        return false;
    }

    void operator= (const std::string& s) { set(s); }
    void operator= (const char* s) { set(s); }

    template<typename T2> void operator= (const T2& x) { val = x; }
    operator T() { return val; }

    std::string str() const
    {
        static constexpr char lut_bin_nib[] =
        {
            "0000" "1000" "0100" "1100" "0010" "1010" "0110" "1110"
            "0001" "1001" "0101" "1101" "0011" "1011" "0111" "1111"
        };
        std::string s = "#";
        switch (type)
        {
            case T_DECIMAL:
            {
                s = std::to_string(val);
                if (s.length() < num_chars) { std::string t(num_chars - s.length(), ' '); s = t + s; }
                return s;
            } break;
            case T_HEX:
            {
                // try to fit it into "num_chars"
                // if it's shorter - add zeros in front
                //size_t i = 0;
                //if (val == 0) { s = "0";}
                std::string t;
                T x = val;
                while (x)
                {
                    const uint8_t b = x&0xFF;
                    x = (x>>8);
                    const uint8_t b1 = (b >> 4);
                    const uint8_t b0 = (b&0x0F);
                    static constexpr char lut_hex[16] =
                    {
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
                    };
                    t += lut_hex[b0];
                    t += lut_hex[b1];
                }
                size_t n = t.length();
                size_t i = n;
                while (i < num_chars) { t += '0'; ++i; }
                i = 0; n = t.length();
                s = "0x";
                while (i < n) { ++i; s += t[n-i]; }
                return s;
            }
            case T_BIN2:
                s = "0b";
                [[fallthrough]];
            case T_BIN:
            {
                std::string t;
                T x = val;
                while (x)
                {
                    const uint8_t b = x&0xFF;
                    x = (x>>8);
                    const uint8_t b1 = (b >> 4)<<2;
                    const uint8_t b0 = (b&0x0F)<<2;
                    t += lut_bin_nib[b0];
                    t += lut_bin_nib[b0+1];
                    t += lut_bin_nib[b0+2];
                    t += lut_bin_nib[b0+3];
                    t += lut_bin_nib[b1];
                    t += lut_bin_nib[b1+1];
                    t += lut_bin_nib[b1+2];
                    t += lut_bin_nib[b1+3];
                }
                size_t n = t.length();
                size_t i = n;
                while (i < num_chars) { t += '0'; ++i; }
                i = 0; n = t.length();
                while (i < n) { ++i; s += t[n-i]; }
                return s;
            }
            case T_ERROR: break;
            // default: don't put a default!
        };
        s = "<error>";
        std::cerr << "* Error: bad fumber conversion!\n";
        throw std::logic_error("this fumber was bad, cannot convert it to string.");
        return s;
    }
};

struct z_logger
{
    bool open(const std::string& fn) { of.open(fn); return of.is_open(); }
    template<class... Ts> void operator() (const Ts& ... args)
    {
        (std::cout << ... << args);
        if (of.is_open())
        {
            (of << ... << args);
            //(std::cout << ... << args);
            if (of.tellp() > 1024*1024*4)
            {
                std::cerr << " * CLOSING LOG FILE * ";
                of.close();
            }
        }
    }

    std::ofstream of;
};

static z_logger logger;
using fumber64_t = fumber_t<uint64_t>;

template<typename T, std::ostream &os = std::cout>
void print_vec(const std::vector<T> &vec, const std::string name)
{
    os << name << "[] =\n{\n";
    std::size_t j = 0;
    for (const auto& i: vec)
    {
        os << "  [" << j << "] " << i << "\n"; ++j;
    }
    os << "}\n";
}

inline static void str_replace(std::string& subject, const std::string& search, const std::string& replace)
{
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos)
    {
         subject.replace(pos, search.length(), replace);
         pos += replace.length();
    }
}

struct skip_first_delimiter
{
    skip_first_delimiter(const std::string _what) : once(true), what(_what) {}
    std::string operator()()
    {
        const bool x = once;
        once = false;
        if (x == false) { return what; }
        return std::string("");
    }
    bool once;
    std::string what;
};

inline static bool get_default_cfg_fn(std::string &fn, const std::vector<std::string> &v_altdir, const std::string what = "config file")
{
    // fn should contain a filename (like config.cfg)
    // this checks if it exists in any of the paths in v_altdir
    // return true on success and the result is in fn
    std::ostringstream os;
    namespace fs = std::filesystem;

    //for (const std::string &alt : v_altdir)
    // search backwards
    size_t i = v_altdir.size();
    while (i)
    {
        --i;
        const std::string &alt = v_altdir.at(i);
        fs::path p = fs::absolute(alt);
        p /= fn;
        std::cout << "- checking: " << p.string() << "\n";
        if (fs::exists(p)) { fn = p.string(); return true; }
        else { os << "Error: " << what << ' ' << p << " does not exist.\n"; }
    }
    std::cerr << os.str();
    return false;
}
inline static std::vector<std::string> split_std_path(const std::filesystem::path p)
{
    #ifdef _WIN32
    constexpr char sep = ';';
    #else
    constexpr char sep = ':';
    #endif // _WIN32
    std::vector<std::string> res;
    const std::string ppp = p.string();
    std::string buf;
    const size_t n = ppp.size();
    size_t i = 0;
    while (i < n)
    {
        const char c = ppp[i];
        if (c == sep)
        {
            if (!buf.empty()) { res.push_back(buf); buf.clear(); }
        }
        else { buf += c; }
        ++i;
    }
    if (!buf.empty()) { res.push_back(buf); buf.clear(); }
    return res;
}
inline static void add_lookup_dir(std::vector<std::string> &v, const std::string_view dir, const std::string_view appname)
{
    // join "dir/appname", if it exists and is not already in v - add it
    std::filesystem::path p = dir;
    p /= appname;
    if (std::find(v.begin(), v.end(), p.string()) == std::end(v))
    {
        if (std::filesystem::exists(p)) { v.push_back(p.string()); }
    }
}

/// Get a path suitable for storing configs for this user, the path might not exist! empty path on fail.
std::filesystem::path get_userconfig_path(const std::string appname = "");
/// Get a path suitable for storing read-only data, the path might not exist! empty path on fail.
std::filesystem::path get_data_path(const std::string appname = "");


struct crc32a
{
    // http://www.hackersdelight.org/hdcodetxt/crc.c.txt

    uint32_t crc;
    uint32_t reverse(uint32_t x)
    {
        x = ((x & 0x55555555) <<  1) | ((x >>  1) & 0x55555555);
        x = ((x & 0x33333333) <<  2) | ((x >>  2) & 0x33333333);
        x = ((x & 0x0F0F0F0F) <<  4) | ((x >>  4) & 0x0F0F0F0F);
        x = (x << 24) | ((x & 0xFF00) << 8) | ((x >> 8) & 0xFF00) | (x >> 24);
        return x;
    }
    void init()
    {
        crc = 0;
        crci = 0xFFFFFFFF;
        b = 0xFFFFFFFF;
    }
    uint32_t calc(uint32_t word, const unsigned nbits = 16)
    {
        b = reverse(word);
        unsigned j = 0;
        while (j < nbits) // 8 times for 8 bits? maybe it should be 16 for the xmega?
        {
            union
            {
                uint32_t ui;
                int32_t si;
            };
            ui = crci ^ b;
            if (si < 0)
            {
                crci = (crci << 1) ^ (0x04C11DB7);
            }
            else
            {
                crci = (crci << 1);
            }
            b = (b << 1); // next bit
            ++j;
        }
        return reverse(~crci);
    }
    void process(const uint8_t *p, size_t n)
    {
        size_t i = 0;
        while (i < n)
        {
            crc = calc(*p);
            ++p; ++i;
        }
    }
    private:
    uint32_t crci, b;
};

struct ghcl_t
{
    // ghetto HCL
    float hf(float _h)
    {
        _h -= std::floor(_h);
        _h *= 3.f;
        if      (_h >= 2.f) { _h = 0.f; }
        else if (_h >  1.f) { _h = (2.f - _h); }
        //_h = std::pow((_h + _h * (1.f-_h)), 1.414f);
        _h = std::pow(_h, (1.f - _h * _h));
        return _h;
    }
    bool calc_rgb(float &r, float &g, float &b)
    {
        if (l <= 0.f)
        {
            r = 0.f; g = 0.f; b = 0.f;
            if (l < 0.f) { return false; }
            return true;
        }
        if (c <= 0.f)
        {
            r = l;
            g = l;
            b = l;
            if (l >= 1.f) { return false; }
            if (c <  0.f) { return false; }
            return true;
        }
        r = hf(h + 0.f/3.f);
        g = hf(h + 1.f/3.f);
        b = hf(h + 2.f/3.f);
        //r = std::pow(r, 0.707); g = std::pow(g, 0.707); b = std::pow(b, 0.707);
        //constexpr float zdb = 0.367879441171f;
        //constexpr float zdb = 0.0722f;
        //r /= (0.2126f + zdb);
        //g /= (0.7152f + zdb);
        //b /= (0.0722f + zdb);
        float Y;
        //Y = (0.2126f * r) + (0.7152f * g) + (0.0722f * b);
        Y = hf(1.f/6.f) * 1.f;
        r = Y + c * (r-Y);
        g = Y + c * (g-Y);
        b = Y + c * (b-Y);
        Y = (0.2126f * r) + (0.7152f * g) + (0.0722f * b);
        float G = l/Y;
        r *= G;
        g *= G;
        b *= G;
        if (b > 1.f) { return false; }
        if (r > 1.f) { return false; }
        if (g > 1.f) { return false; }
        return true;
    }
    float h, c, l;
};

inline static void print_version(std::ostream &os, const int v)
{
    os << (v / 100) << '.' << std::setw(2) << std::setfill('0') << (v %100);
}

/*

100000000 (100M)
10000000 (10M)
1000000 (1M)
100000 (100k)
10000 (10k)
1000 (1k)
100
10
1
01 (100m)
001 (10m)
0001 (1m)
00001 (100u)
000001 (10u)
0000001 (1u)
00000001 (100n)
000000001 (10n)
0000000001 (1n)
00000000001 (100p)
000000000001 (10p)
0000000000001 (1p)
00000000000001 (100f)
000000000000001 (10f)
0000000000000001 (1f)
*/
static const std::string mu_sign = "μ";
struct PassiveValueParser
{
    std::stringstream ss;
    std::string _val;
    std::string _val2;
    std::string normalized;
    std::string optimized;
    std::string remainder;
    int64_t v;
    double d;
    double nrml;
    int16_t ne;
    int16_t e;
    int16_t upe;
    size_t _skip;
    int16_t fz;
    int16_t lzad;
    int16_t zae;
    std::vector<std::string> *_pvec_units;

    bool is_unit_prefix(const std::string_view s)
    {
        upe = 0;
        _skip = 0;
        if (s.empty()) { return false; }
        const char c = s[0];
        if (s.substr(0, mu_sign.length()) == mu_sign) { upe = -6; _skip = mu_sign.length()-1; return true; }
        switch (c)
        {
            case 'f': upe = -15; break;
            case 'p': upe = -12; break;
            case 'n': upe = -9; break;
            //case 'μ': [[fallthrough]]
            case 'u': upe = -6; break;
            case 'm': upe = -3; break;
            case 'k': upe = 3; break;
            case 'M': upe = 6; break;
            case 'G': upe = 9; break;
            default: return false;
        };
        _skip = sizeof(char)-1;
        return true;
    }
    const std::string get_unit_prefix(const int16_t eee)
    {
        std::string s = "?";
        switch (eee)
        {
            case -13:  s = 'f'; break;
            case -12:
            case -11:
            case -10: s = 'p'; break;
            case -9:
            case -8:
            case -7: s = 'n'; break;
            case -6:
            case -5:
            case -4: s = mu_sign; break;
            case -3:
            case -2:
            case -1: s = 'm'; break;
            case 0:
            case 1:
            case 2: s = ""; break;
            case 3:
            case 4:
            case 5: s = 'k'; break;
            case 6:
            case 7:
            case 8: s = 'M'; break;
            case 9:
            case 10:
            case 11: s = 'G'; break;
        }
        return s;
    }
    bool is_separator(const char c)
    {
        switch (c)
        {
            case ' ': break;
            case '/': break;
            case '@': break;
            default: return false;
        }
        return true;
    }
    bool is_unit(const std::string_view s)
    {
        if (s.empty()) { return false; }
        if (_pvec_units == nullptr) { return false; }
        if (_pvec_units->empty()) { return false; }
        for (const std::string_view& u : *_pvec_units)
        {
            if (u.length() > s.length()) { continue; }
            if (u == s.substr(0, u.length())) { _skip = u.length()-1; return true; }
        }
        return false;
    }
    bool parse(const std::string val, std::vector<std::string> *pvec_units)
    {
        v = -1;
        nrml = -1.0;
        normalized.clear();
        optimized.clear();
        remainder.clear();
        ne = 0;
        upe = 0;
        e = 0;
        fz = 0;
        lzad = 0;
        zae = 0;
        _val.clear();
        _val2.clear();
        int64_t ii = 0;
        size_t i = 0;
        const size_t N = val.length();
        uint8_t state = 0;
        if (val.empty()) { state = 0xFF; }
        bool lz = true;
        int lzc = 0;
        _pvec_units = pvec_units;
        while (i < N)
        {
            char c = val[i];
            const bool isdigit = ((c >= '0') && (c <= '9'));
            if ((i == 0) && (!isdigit) && (c != '.')) { state = 0xFF; _val = val.substr(i); break; }
            bool gotdot = false;
            bool gotunit = false;
            bool gotupe = false;
            switch (state)
            {
                // ### 27.4 k JUNK
                // >>> 00112344444
                // ### 27k4 JUNK
                // >>> 003444444
                // ### 27k/JUNK
                // >>> 00344444
                // ### JUNK
                // >>> 4444
                case 0:
                    if (isdigit)
                    {
                        ii = 10*ii+(c-'0'); ++e;
                        if (lzc >= 1) { state = 0xFF; }
                        if (lz && (c == '0')) { ++lzc; }
                        else { lz = false; }
                    }
                    else
                    {
                        if      (c == '.') { state = 1; gotdot = true; e -= lzc; lz = true; }
                        else if (is_unit(val.substr(i))) { state = 3; gotunit = true; e -= lzc; }
                        //else if (c == ' ') { state = 2; if (upe != 0) { state = 4; } }
                        else if (is_separator(c)) { state = 2; if (upe != 0) { state = 4; e -= lzc; } }
                        else if (is_unit_prefix(val.substr(i))) { state = 3; gotupe = true; i += _skip; e -= lzc; }
                        else { state = 0xFF; }
                    }
                    break;
                case 1: // after the dot
                    if (isdigit)
                    {
                        ii = 10*ii+(c-'0'); ++e; ++fz;
                        if (lz && (c == '0')) { ++lzad; } else { lz = false; }
                        if (c == '0') { ++zae; } else { zae = 0; }
                    }
                    //else if (c == ' ') { state = 2; }
                    else if (is_separator(c)) { state = 2; }
                    else if (is_unit_prefix(val.substr(i))) { state = 3; if (gotupe) { state = 0xFF; } gotupe = true; i += _skip; }
                    else if (c == '.') { state = 0xFF; }
                    else { state = 4; }
                    break;
                case 2:
                    // no more digits or dots allowed
                    if (is_unit_prefix(val.substr(i))) { state = 3; gotupe = true; i += _skip; }
                    //else if (isdigit || (c == '.')) { state = 0xFF; }
                    else if (c == '.') { state = 0xFF; }
                    else { state = 4; }
                    break;
                case 3:
                    // the unit prefix may have been used instead of a dot
                    if (!gotdot && isdigit)
                    {
                        ii = 10*ii+(c-'0'); ++e; ++fz;
                        if (lz && (c == '0')) { ++lzad; } else { lz = false; }
                        if (c == '0') { ++zae; } else { zae = 0; }
                    }
                    else if (c == '.') { state = 0xFF; }
                    else { state = 4; }
                    break;
            }
            if (state >= 4) { _val = val.substr(i); break; }
            ++i;
        }
        if ((e+fz) < 1) { state = 0xFF; }
        //std::cout << " *** " << ii << " *** ";
        if (state != 0xFF)
        {
            v = ii;
            d = ii;

            #if 0
            std::cout  << '\"'<< val << "\"\t: \t"
                << ii << ' ' << e << " \t* " << upe
                << " \t# \"" << _val << '\"'
                << "\n";
            #else
            //std::cout  << '\"'<< val << "\"\t:";
            //<< ii << "\t";
            //proc();
            #endif
        }
        else
        {
            //std::cout << '\"' << val << "\"\tbad." << '\n';
            return false;
        }
        return true;
    }
    int64_t _pow(int16_t eee) { int64_t x = 1; while (eee > 0) { --eee; x *= 10; } return x; }
    constexpr int16_t _min(int16_t a, int16_t b) { return (a < b ? a : b); }
    constexpr int16_t _max(int16_t a, int16_t b) { return (a < b ? b : a); }
    constexpr int16_t _r(int16_t x) { return (x/3)*3; }
    void proc(bool verbose = false)
    {
        d /= std::pow(10.0,-upe+fz);
        ne = (-upe+fz);
        int16_t zz = 0; //, yy = 0;
        int16_t yy2 = 0;
        #if 0
        {
            double x = 1000.0;
            size_t j = 0;
            if (d > 1.0)
            {
                while (j < 3) { ++j; if (d >= x) { ++zz; ne -=3; } else { break; } x *= 1000.0; }
                nrml = d / std::pow(10.0, zz*3);
            }
            else if (d > 0.0)
            {
                x = 1.0;
                while (j < 5) { ++j; if (d < x) { --zz; ne += 3; } else { break; } x = 1.0 * std::pow(10.0,-3.0*j); }
                nrml = d / std::pow(10.0, zz*3);
            }
            else { nrml = 0.0; }
        }
        #elif 1
        {
            if (d > 1.0)
            {
                int16_t yy = e;
                yy = e-ne;
                yy2 = yy;
                while (yy > 3) { yy -= 3; ++zz; ne += 3; }
            }
            else if (d > 0.0)
            {
                // ne = (-upe+fz);
                #if 1
                int yy = 0;
                int16_t i = e-fz;
                {
                    //++e;
                    int16_t b = 3;
                    yy = -upe + _r(-i+b)+_r(fz-1 +b*0-zae-0*lzad) - _min(_max(i-3,0), 3);
                    //yy = 18;
                }
                #else
                //int16_t yy = _r(ne+2) +2+ _min(-e,-3) + _min(fz,2);
                //int16_t yy = _r(ne+2) +0+ _r(-e+2)*0 - 1*_r(fz-1);
                //e = _max(1,e);
                //int16_t fze= fz-e;
                int16_t i = e-fz;
                //int16_t ne2 = (fz-upe)-zae+lzad;
                int16_t ne2 = (_max((i),(-i+fz-zae+lzad)))-upe;
                //int16_t e2 = _min(+fz-zae+2,5)-e+lzad; //_r(-e*2+2*fz+lzad+2)+_r(0+e-fz)-zae+0*lzad;
                int16_t yy = ne2+-2;
                yy = -i+fz+ne+1*lzad-0*zae;
                //int16_t yy = -e+ne+3+std::min<int16_t>(fz,3);
                //yy = ne + std::min<int16_t>(fz,-e+fz-3) + 3;
                #endif
                yy2 = yy;
                while (yy >= 3) { yy -=3; --zz; ne -= 3; }
            }
            else { nrml = 0.0; }
            nrml = v / std::pow(10.0, ne);
        }
        #endif

        #if 1
        std::string tmp;
        //if (upe < 0)
        {
            ss.str(""); ss.clear();

            if (upe >= 0)
            {
                ss << std::defaultfloat << std::dec << std::right << std::setw(12) << std::setfill('0') << std::setprecision(16) << d;
                tmp += ss.str();

                //size_t j = tmp.length(); while (j--) { _val2 += tmp[j]; }
                _val2 = tmp;
                _val2 += ' ' + _val;
            }
            else
            {
                ss << std::defaultfloat << std::fixed << std::setprecision(16) << d;
                tmp += ss.str();
                _val2 = tmp + ' ' + _val;
            }

            normalized = tmp;
            remainder = _val;
            tmp += ' ' +  _val;
            _val = tmp;
        }
        ss.str(""); ss.clear();
        ss << std::defaultfloat << std::setprecision(6) << nrml << get_unit_prefix(zz*3);
        optimized = ss.str();
        #endif
        #if 1
        //std::cout  << _val
        //std::cout  << std::fixed << std::setprecision(15) << d
        if (verbose)
        {
        std::cout << std::setw(16) << v
        << std::setw(3) << upe << std::setw(3) << e << std::setw(3) << fz
        << " yy2 = " << std::setw(5) << yy2 //-(-upe+fz)
        << " ne = " << std::setw(5) << -ne
        << std::setw(5) << ":" << std::setw(28) << std::setprecision(16) << std::fixed << d
        << " (" << std::defaultfloat << std::setprecision(6) << nrml << get_unit_prefix(zz*3) << ") ";
        }
        //<< '\n';
        #endif
        //std::cout << _val << '\n';
    }
};

#endif // UTILS_HPP_INCLUDED
