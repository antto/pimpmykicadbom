#!/bin/sh
# pass linuxdeploy as first argument please

if [ $1 ]; then
    rm AppDir/usr/bin/pimpmykicadbom
    mkdir -p AppDir/usr/bin
    mkdir -p AppDir/usr/lib
    mkdir -p AppDir/usr/share/pimpmykicadbom
    cp ../fp2pkg_regex.txt AppDir/usr/share/pimpmykicadbom/.
    # ln -s AppDir/usr/bin/pimpmykicadbom AppDir/AppRun
    $1 --appdir AppDir -e ../bin/linuxRelease/pimpmykicadbom --output appimage
else
    echo "please pass the location of linuxdeploy.AppImage"
fi
