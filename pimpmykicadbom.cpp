#include <vector>
#include <cassert>
#include <map>
#include <set>
#include <functional>
#include <regex>
#include <tinyxml2.h>
#include "utils.hpp"

using namespace tinyxml2;
using std::cout, std::cerr, std::ofstream;
using std::string, std::to_string, std::size_t, std::vector;
using std::string_view;
namespace fs = std::filesystem;

constexpr char appname[] = "pimpmykicadbom";
constexpr char appwebsite[] = "https://gitlab.com/antto/pimpmykicadbom";
constexpr int appversion = 102;

// ###################################################################




// ###################################################################

namespace as_xml
{
enum PARSER_RETCODE
{
    RC_SUCCESS,
    RC_ERROR_NULLPTR,
    RC_ERROR_NO_ATTR,
    RC_ERROR_ELEMENT,
    RC_ERROR_EMPTY_ELEMENT,
    RC_ERROR_INSUFFICIENT_DATA,

    RC_SKIP, // for debugging
    RC_NO_CHILDREN,
    RC_NO_NEXT
};


struct attr_t
{
    string aname, aval;
};
/// helper object for parsing elements
struct element_t
{
    element_t() : valid(false), has_attr(false), pe(nullptr), level(0), path(".") {}
    string ename, etext;
    bool valid, has_attr;
    vector<attr_t> v_attr;
    int doc_line;
    const XMLElement *pe;
    size_t level;
    string path;

    //! returns RC_SUCCESS or RC_NO_CHILDREN
    PARSER_RETCODE go_in_and_read(vector<attr_t> *v_a = nullptr, const string& where = "")
    {
        if (pe == nullptr) { throw std::logic_error("go_in_and_read on a nullptr?"); return RC_ERROR_NULLPTR; }
        const XMLElement *e = pe->FirstChildElement();
        if (e != nullptr)
        {
            ++level;
            pe = e;
            const string old_ename = ename;
            if (read(pe, v_a))
            {
                path += '/';
                if (where.empty()) { path += old_ename; }
                else { path += where; }
                return RC_SUCCESS;
            }
            return RC_ERROR_ELEMENT;
        }
        return RC_NO_CHILDREN;
    }
    //! returns RC_SUCCESS or RC_NO_NEXT
    PARSER_RETCODE go_down_and_read(vector<attr_t> *v_a = nullptr)
    {
        if (pe == nullptr) { throw std::logic_error("go_down_and_read on a nullptr?"); return RC_ERROR_NULLPTR; }
        const XMLElement *e = pe->NextSiblingElement();
        if (e != nullptr)
        {
            pe = e;
            if (read(pe, v_a)) { return RC_SUCCESS; }
            return RC_ERROR_ELEMENT;
        }
        return RC_NO_NEXT;
    }
    bool read(const XMLElement *e, vector<attr_t> *v_a = nullptr)
    {
        valid = false;
        has_attr = false;
        doc_line = -1;
        pe = nullptr;
        v_attr.clear();
        if (e == nullptr) { return false; }
        pe = e;
        doc_line = e->GetLineNum();
        bool warn1 = false;
        if (!get_element_name(e, ename)) { return false; }
        if (!get_element_text(e, etext))
        {
            // i think if there's no text, then there *must* be children elements
            //cout << "* Warning: requesting text from element which does not contain text:\n";
            //print_element(e);
            if (e->FirstChildElement() == nullptr)
            {
                // i thought this was not allowed in SVD but atsame54n20a.svd has
                // <description/>
                //cout << "* Warning: element \"" << ename << "\" with no text and no children (line: " << e->GetLineNum() << ").\n";
                warn1 = true;
                etext = "<empty>";
                //return false;
            }
        }
        #if 0
        if (str_compare(ename, (string)(e->Value())) == false)
        {
            cout << "* Warning: element \"" << ename << "\" has value \"" << e->Value() << "\" (line: " << e->GetLineNum() << ").\n";
        }
        #endif // 0
        // check for attributes... most elements don't have them
        const XMLAttribute *a = e->FirstAttribute();
        if (a != nullptr)
        {
            has_attr = true;
            // load the attributes
            while (a != nullptr)
            {
                attr_t attr = { a->Name(), a->Value() };
                v_attr.push_back(attr);
                a = a->Next();
            }
            if (v_a != nullptr) { *v_a = v_attr; }
            warn1 = false;
        }
        else
        {
            if (warn1)  { cout << "* Warning: element \"" << ename << "\" with no text/attributes and no children (line: " << doc_line << ").\n"; }
            warn1 = false;
        }
        if (warn1)      { cout << "* Warning: element \"" << ename << "\" with no text and no children (line: " << doc_line << ").\n"; }
        valid = true;
        return true;
    }

    template<typename Tstr, typename T>
    bool get(const Tstr what, T& target)
    {
        if (valid)
        {
            if (str_compare(what, ename))
            {
                if constexpr (std::is_same<T, std::string>::value)
                {
                    // STM32 SVDs have newlines often
                    string tmp = etext;
                    if (str_rem_newlines(tmp))
                    {
                        cout << "* W: element " << ename << " had multi-line text.\n";
                        //cout << "* W: element " << ename << " had multi-line text: \"" << tmp << "\"\n";
                    }
                    target = tmp;
                    return true;
                }
                target = etext;
                return true;
            }
            return false;
        }
        throw std::logic_error("get(x,y) on an invalid element_t.");
        return false;
    }
    template<typename Tstr>
    bool get(const Tstr what)
    {
        if (valid)
        {
            if (str_compare(what, ename)) { return true; }
            return false;
        }
        throw std::logic_error("get(x) on an invalid svde.");
        return false;
    }

    void print(std::ostream& os)
    {
        if (valid == false) { os << "element_t<INVALID>\n"; return; }
        fumber64_t lnum; lnum = "0"; lnum.num_chars = 6;
        lnum = doc_line;
        string tab;
        if (level > 0) { tab = string(level*4, ' '); }
        os << lnum.str() << tab << " <e> \"" << ename << "\" ::: \"" << etext << "\"\n";
        for (const attr_t& a : v_attr)
        {
            os << tab <<"       [a] \"" << a.aname << "\" = \"" << a.aval << "\"\n";
        }
    }
    void print_w_unhandled(std::ostream& os)
    {
        os << "Warning: unhandled element(level=" << level << "): " << ename << "\n";
    }
};

} // namespace as_xml

constexpr char g_config_field[] = "MEH_FIXME";

using namespace as_xml;

enum COLUMN_ORDER : size_t
{
    // Note: this does not show the default ordering!

    CO_REF  = 0,    // annotated references, like "C4 C25 C27"
    CO_PV,          // package:value
    CO_PKG,         // package
    CO_VAL,         // value
    CO_FP,          // footprint
    CO_COUNT,       // quantity per board
    CO_TOTAL,       // total quantity
    CO_DESC,        // symbol description (from the lib)
    CO_DATASH,      // datasheet
    CO_SYMB,        // symbol
    CO_SRCBOM,      // in a merged BOM, where do the components come from
    CO_PARSEDVAL,   // parsed value
    CO_CHECKB,      // checkbox column
    CO_ALLCUSTOM,   // any remaining custom fields go here .. hm

    CO__X,          // keep last
    CO__N = 256, // keep at bottom, custom fields use this as offset!
};
static_assert(CO__X <= CO__N);
const string g_column_names[][3] =
{
    // name (for HTML), name (for CSV), title (for tooltip)
    { "Ref",            "Ref",              "Reference designator"  },
    { "Package:Value",  "Package:Value",    "Auto-generated"        },
    { "Package",        "Package",          "Auto-generated"        },
    { "Value",          "Value",            "Component value"       },
    { "Footprint",      "Footprint",        "Component footprint"   },
    { "Qty",            "Qty(1)",           "Quantity (one board)"  },
    { "QTY",            "Total",            "Total Quantity"        },
    { "Description",    "Description",      "(from symbol library)" },
    { "Data<br/>Sheet", "Datasheet",        "" },
    { "Symbol",         "Symbol",           "Schematic symbol"      },
    { "Source<br/>BOM", "SourceBOM",        "Auto-generated"        },
    { "Parsed Value",   "ParsedValue",      "Parsed value"          },
    { "Chk",            "CheckBox",         "A check-box"           },
    { "*AllCustom*",    "*AllCustom*",      "Unspecified cfield"    } // this one shouldn't be seen
};
static_assert(CO__X == std::size(g_column_names));
enum COLUMN_SORT_ORDER : size_t
{
    CSO_REF  = 0,   // annotated references, like "C4 C25 C27"
    CSO_REFDES,     // RefDes, like "C"
    CSO_PKG,        // Package
    CSO_VALUE,      // Value
    CSO_DESC,       // symbol description (from the lib)
    CSO_DATASHEET,  // datasheet
    CSO_SYMBOL,     // symbol
    CSO_PARSEDVAL,  // parsed value
    CSO_DNF,        // Do-Not-Fit
    //CSO_ALLCUSTOM,   // any remaining custom fields go here .. hm

    CSO__X,         // keep last
    CSO__N = 256, // keep at bottom, custom fields use this as offset!
};
static_assert(CSO__X <= CSO__N);
const string g_cso_names[][2] =
{
    { "Ref",        "Annotated References"              },
    { "RefDes",     "Reference Designator"              },
    { "Package",    "Package name (auto-generated)"     },
    { "Value",      "Component Value"                   },
    { "Description","Symbol Description (from the Lib)" },
    { "Datasheet",  "Component Datasheet"               },
    { "Symbol",     "Symbol name"                       },
    { "ParsedVal",  "Component Parsed value"            },
    { "DNF",        "The Do-Not-Fit property"           }
};
static_assert(CSO__X == std::size(g_cso_names));
enum COLUMN_GROUP_BY// : size_t
{
    CGB_REFDES = 0, // RefDes, like "C"
    CGB_FOOTPRINT,  // Footprint (no lib)
    CGB_FPLIB,      // FootprintLib
    CGB_SYMBOL,     // Symbol (no lib)
    CGB_SYMLIB,     // SymbolLib
    CGB_DESC,       // symbol description (from the lib)
    CGB_VALUE,      // Value
    CGB_DATASHEET,  // datasheet
    CGB_PKG,        // Package
    CGB_PARSEDVAL,  // Parsed Value
    CGB_DNF,        // Do-Not-Fit (the bool)

    CGB__X,         // keep last
    CGB__N = 256, // keep at bottom, custom fields use this as offset!
};
static_assert(CGB__X <= CGB__N);
const string g_cgb_names[][2] =
{
    // name (for HTML), title (for tooltip)
    { "RefDes",     "Reference Designator"              },
    { "Footprint",  "Footprint name (without Lib)"      },
    { "FpLib",      "Footprint Library"                 },
    { "Symbol",     "Symbol name (no Lib)"              },
    { "SymLib",     "Symbol Library"                    },
    { "Description","Symbol Description (from the Lib)" },
    { "Value",      "Component Value"                   },
    { "Datasheet",  "Component Datasheet"               },
    { "Package",    "Package name (auto-generated)"     },
    { "ParsedValue","Decoded from the Value"            },
    { "DNF",        "The Do-Not-Fit property"           }
};
static_assert(CGB__X == std::size(g_cgb_names));
enum COLUMN_COLOR_INFLUENCE// : size_t
{
    CCI_REFDES = 0, // RefDes, like "C"
    CCI_FOOTPRINT,  // Footprint (no lib)
    CCI_FPLIB,      // FootprintLib
    CCI_SYMBOL,     // Symbol (no lib)
    CCI_SYMLIB,     // SymbolLib
    CCI_DESC,       // symbol description (from the lib)
    CCI_VALUE,      // Value
    CCI_DATASHEET,  // datasheet
    CCI_PKG,        // Package
    CCI_PARSEDVAL,  // Parsed Value
    CCI_DNF,        // Do-Not-Fit (the bool)

    CCI__N = 256, // keep at bottom, custom fields use this as offset!
};
struct column_order_t
{
    size_t i;
    bool used;
    string custom;
};
struct re_instr_t /// regex instructions
{
    string aaa;
    string bbb;
};
struct CFG
{
    // configuration
    PARSER_RETCODE parse(const XMLElement *root)
    {
        if (root == nullptr) { return RC_ERROR_NULLPTR; }
        PARSER_RETCODE res = RC_SUCCESS;
        element_t root_elem;
        element_t elem0, elem1;
        if (!root_elem.read(root)) { return RC_ERROR_NULLPTR; } // TODO: fix this error code
        elem0 = root_elem;
        res = elem0.go_in_and_read(nullptr, "pimpmykicadbom_config");
        if (res == RC_NO_CHILDREN) { return RC_ERROR_ELEMENT; }

        string nickname;
        while (true)
        {
            if      (elem0.get("output_subpath", subpath)) {}
            else if (elem0.get("pv_delimiter", pv_delim)) {}
            else if (elem0.get("csv_separator", csv_delim)) {}
            else if (elem0.get("output_name_format", ofn_fmt)) {}
            else if (elem0.get("config_nickname", nickname))
            {
                if (!name.empty()) { name += " > "; }
                if (nickname.empty()) { nickname = "[unnamed]"; }
                name += nickname;
            }
            else if (elem0.get("dnf", dnf_val))
            {
                // the custom field name should be an attribute
                if (elem0.has_attr)
                {
                    for (const attr_t &aaa : elem0.v_attr)
                    {
                        if (str_compare(aaa.aname, "name")) { dnf_cfname = aaa.aval; }
                        else
                        {
                            cout << "* W: unknown column field: \"" << aaa.aval << "\"\n";
                        }
                    }
                }
                else
                {
                    cerr << "Error: CFG: dnf must have attributes.\n";
                    return RC_ERROR_NO_ATTR;
                }
            }
            else if (elem0.get("column_order"))
            {
                PARSER_RETCODE res1;
                elem1 = elem0; res1 = elem1.go_in_and_read();
                if (res1 == RC_SUCCESS)
                {
                    v_colorder.clear();
                    while (true)
                    {
                        if (elem1.get("col"))
                        {
                            column_order_t col;
                            col.i = CO__N;
                            col.used = true;
                            if (elem1.has_attr)
                            {
                                for (const auto &aaa : elem1.v_attr)
                                {
                                    if      (str_compare(aaa.aname, "used"))
                                    {
                                        try
                                        {
                                            col.used = std::stol(aaa.aval);
                                        }
                                        catch (const std::invalid_argument &ia)
                                        {
                                            cerr << "Error: invalid argument: " << ia.what() << "\n";
                                            return RC_ERROR_ELEMENT;
                                        }
                                    }
                                    else if (str_compare(aaa.aname, "name"))
                                    {
                                        //cout << "col: " << aaa.aval << "\n";
                                        if      (str_compare(aaa.aval, "Ref"))          { col.i = CO_REF; }
                                        else if (str_compare(aaa.aval, "PV"))           { col.i = CO_PV; }
                                        else if (str_compare(aaa.aval, "Package"))      { col.i = CO_PKG; }
                                        else if (str_compare(aaa.aval, "Value"))        { col.i = CO_VAL; }
                                        else if (str_compare(aaa.aval, "Footprint"))    { col.i = CO_FP; }
                                        else if (str_compare(aaa.aval, "Quantity"))     { col.i = CO_COUNT; }
                                        else if (str_compare(aaa.aval, "QTotal"))       { col.i = CO_TOTAL; }
                                        else if (str_compare(aaa.aval, "Description"))  { col.i = CO_DESC; }
                                        else if (str_compare(aaa.aval, "Datasheet"))    { col.i = CO_DATASH; }
                                        else if (str_compare(aaa.aval, "SYMBOL"))       { col.i = CO_SYMB; }
                                        else if (str_compare(aaa.aval, "SrcBOM"))       { col.i = CO_SRCBOM; }
                                        else if (str_compare(aaa.aval, "ParsedVAL"))    { col.i = CO_PARSEDVAL; }
                                        else if (str_compare(aaa.aval, "Check"))        { col.i = CO_CHECKB; }
                                        else if (str_compare(aaa.aval, "AllCustom"))    { col.i = CO_ALLCUSTOM; }
                                        else
                                        {
                                            cout << "* W: unknown column field: \"" << aaa.aval << "\"\n";
                                        }
                                    }
                                    else if (str_compare(aaa.aname, "custom"))
                                    {
                                        col.custom = aaa.aval;
                                        if (col.custom.empty())
                                        {
                                            cerr << "* Error: custom column empty.\n";
                                            return RC_ERROR_ELEMENT;
                                        }
                                    }
                                    else
                                    {
                                        logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem1.path, "\n");
                                    }
                                }
                                v_colorder.push_back(col);
                            }
                            else
                            {
                                logger("/// E: colum_order:col attributes are required (line: ",elem1.doc_line,").\n");
                                return RC_ERROR_NO_ATTR;
                            }
                        }
                        else
                        {
                            elem1.print_w_unhandled(cout);
                        }
                        res1 = elem1.go_down_and_read();
                        if (res1 == RC_NO_NEXT) { break; }
                    }
                }
                else
                {
                    cerr << "Error: CFG: bad column_order.\n";
                    return res1;
                }
            }
            else if (elem0.get("sort_order"))
            {
                PARSER_RETCODE res1;
                elem1 = elem0; res1 = elem1.go_in_and_read();
                if (res1 == RC_SUCCESS)
                {
                    v_sortorder.clear();
                    while (true)
                    {
                        if (elem1.get("col"))
                        {
                            column_order_t col;
                            col.i = CO__N;
                            col.used = true;
                            if (elem1.has_attr)
                            {
                                for (const auto &aaa : elem1.v_attr)
                                {
                                    if      (str_compare(aaa.aname, "used"))
                                    {
                                        try
                                        {
                                            col.used = std::stol(aaa.aval);
                                        }
                                        catch (const std::invalid_argument &ia)
                                        {
                                            cerr << "Error: invalid argument: " << ia.what() << "\n";
                                            return RC_ERROR_ELEMENT;
                                        }
                                    }
                                    else if (str_compare(aaa.aname, "name"))
                                    {
                                        //cout << "- " << aaa.aval << "\n";
                                        if      (str_compare(aaa.aval, "Ref"))          { col.i = CSO_REF; }
                                        else if (str_compare(aaa.aval, "RefDes"))       { col.i = CSO_REFDES; }
                                        else if (str_compare(aaa.aval, "Package"))      { col.i = CSO_PKG; }
                                        else if (str_compare(aaa.aval, "Value"))        { col.i = CSO_VALUE; }
                                        else if (str_compare(aaa.aval, "Description"))  { col.i = CSO_DESC; }
                                        else if (str_compare(aaa.aval, "Datasheet"))    { col.i = CSO_DATASHEET; }
                                        else if (str_compare(aaa.aval, "Symbol"))       { col.i = CSO_SYMBOL; }
                                        //else if (str_compare(aaa.aval, "AllCustom"))    { col.i = CSO_ALLCUSTOM; }
                                        else if (str_compare(aaa.aval, "ParsedVAL"))    { col.i = CSO_PARSEDVAL; }
                                        else if (str_compare(aaa.aval, "DNF"))          { col.i = CSO_DNF; }
                                        else
                                        {
                                            cout << "* W: unknown column field: \"" << aaa.aval << "\"\n";
                                        }
                                    }
                                    else if (str_compare(aaa.aname, "custom"))
                                    {
                                        col.custom = aaa.aval;
                                        if (col.custom.empty())
                                        {
                                            cerr << "* Error: custom column empty.\n";
                                            return RC_ERROR_ELEMENT;
                                        }
                                    }
                                    else
                                    {
                                        logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem1.path, "\n");
                                    }
                                }
                                v_sortorder.push_back(col);
                            }
                            else
                            {
                                logger("/// E: sort_order:col attributes are required (line: ",elem1.doc_line,").\n");
                                return RC_ERROR_NO_ATTR;
                            }
                        }
                        else
                        {
                            elem1.print_w_unhandled(cout);
                        }
                        res1 = elem1.go_down_and_read();
                        if (res1 == RC_NO_NEXT) { break; }
                    }
                }
                else
                {
                    cerr << "Error: CFG: bad sort_order.\n";
                    return res1;
                }
            }
            else if (elem0.get("group_by"))
            {
                const bool cfg_override = !v_groupby.empty();
                PARSER_RETCODE res1;
                elem1 = elem0; res1 = elem1.go_in_and_read();
                if (res1 == RC_SUCCESS)
                {
                    while (true)
                    {
                        if (elem1.get("col"))
                        {
                            column_order_t col;
                            col.i = CO__N;
                            col.used = true;
                            if (elem1.has_attr)
                            {
                                for (const auto &aaa : elem1.v_attr)
                                {
                                    if      (str_compare(aaa.aname, "used"))
                                    {
                                        try
                                        {
                                            col.used = std::stol(aaa.aval);
                                        }
                                        catch (const std::invalid_argument &ia)
                                        {
                                            cerr << "Error: invalid argument: " << ia.what() << "\n";
                                            return RC_ERROR_ELEMENT;
                                        }
                                    }
                                    else if (str_compare(aaa.aname, "name"))
                                    {
                                        //cout << "- " << aaa.aval << "\n";
                                        if      (str_compare(aaa.aval, "RefDes"))       { col.i = CGB_REFDES; }
                                        else if (str_compare(aaa.aval, "Footprint"))    { col.i = CGB_FOOTPRINT; }
                                        else if (str_compare(aaa.aval, "FpLib"))        { col.i = CGB_FPLIB; }
                                        else if (str_compare(aaa.aval, "Symbol"))       { col.i = CGB_SYMBOL; }
                                        else if (str_compare(aaa.aval, "SymLib"))       { col.i = CGB_SYMLIB; }
                                        else if (str_compare(aaa.aval, "Datasheet"))    { col.i = CGB_DATASHEET; }
                                        else if (str_compare(aaa.aval, "Value"))        { col.i = CGB_VALUE; }
                                        else if (str_compare(aaa.aval, "Description"))  { col.i = CGB_DESC; }
                                        else if (str_compare(aaa.aval, "Package"))      { col.i = CGB_PKG; }
                                        else if (str_compare(aaa.aval, "ParsedVAL"))    { col.i = CGB_PARSEDVAL; }
                                        else if (str_compare(aaa.aval, "DNF"))          { col.i = CGB_DNF; }
                                        else
                                        {
                                            cout << "* W: unknown column field: \"" << aaa.aval << "\"\n";
                                        }
                                    }
                                    else if (str_compare(aaa.aname, "custom"))
                                    {
                                        col.custom = aaa.aval;
                                        if (col.custom.empty())
                                        {
                                            cerr << "* Error: custom column empty.\n";
                                            return RC_ERROR_ELEMENT;
                                        }
                                    }
                                    else
                                    {
                                        logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem1.path, "\n");
                                    }
                                }
                                {
                                    bool add_it = true;
                                    if (cfg_override)
                                    {
                                        // check for duplicates here before adding
                                        size_t j = 0;
                                        while (j < v_groupby.size())
                                        {
                                            column_order_t& ccc = v_groupby[j];
                                            if (col.i == ccc.i)
                                            {
                                                if (col.i == CO__N)
                                                {
                                                    if (str_compare_nocs(col.custom, ccc.custom))
                                                    {
                                                        // override the custom field
                                                        add_it = false;
                                                        ccc = col;
                                                        break;
                                                    }
                                                    else
                                                    {
                                                        // add new custom field
                                                        break;
                                                    }
                                                }
                                                else
                                                {
                                                    // override
                                                    add_it = false;
                                                    ccc = col;
                                                    break;
                                                }
                                            }
                                            ++j;
                                        }
                                    }
                                    if (add_it) { v_groupby.push_back(col); }
                                }
                            }
                            else
                            {
                                logger("/// E: group_by:col attributes are required (line: ",elem1.doc_line,").\n");
                                return RC_ERROR_NO_ATTR;
                            }
                        }
                        else
                        {
                            elem1.print_w_unhandled(cout);
                        }
                        res1 = elem1.go_down_and_read();
                        if (res1 == RC_NO_NEXT) { break; }
                    }
                }
                else
                {
                    cerr << "Error: CFG: bad group_by.\n";
                    return res1;
                }
            }
            else if (elem0.get("color_influence"))
            {
                PARSER_RETCODE res1;
                elem1 = elem0; res1 = elem1.go_in_and_read();
                if (res1 == RC_SUCCESS)
                {
                    v_clrinfl.clear();
                    while (true)
                    {
                        if (elem1.get("col"))
                        {
                            column_order_t col;
                            col.i = CO__N;
                            col.used = true;
                            if (elem1.has_attr)
                            {
                                for (const auto &aaa : elem1.v_attr)
                                {
                                    if      (str_compare(aaa.aname, "used"))
                                    {
                                        try
                                        {
                                            col.used = std::stol(aaa.aval);
                                        }
                                        catch (const std::invalid_argument &ia)
                                        {
                                            cerr << "Error: invalid argument: " << ia.what() << "\n";
                                            return RC_ERROR_ELEMENT;
                                        }
                                    }
                                    else if (str_compare(aaa.aname, "name"))
                                    {
                                        //cout << "- " << aaa.aval << "\n";
                                        if      (str_compare(aaa.aval, "RefDes"))       { col.i = CCI_REFDES; }
                                        else if (str_compare(aaa.aval, "Footprint"))    { col.i = CCI_FOOTPRINT; }
                                        else if (str_compare(aaa.aval, "FpLib"))        { col.i = CCI_FPLIB; }
                                        else if (str_compare(aaa.aval, "Symbol"))       { col.i = CCI_SYMBOL; }
                                        else if (str_compare(aaa.aval, "SymLib"))       { col.i = CCI_SYMLIB; }
                                        else if (str_compare(aaa.aval, "Datasheet"))    { col.i = CCI_DATASHEET; }
                                        else if (str_compare(aaa.aval, "Value"))        { col.i = CCI_VALUE; }
                                        else if (str_compare(aaa.aval, "Description"))  { col.i = CCI_DESC; }
                                        else if (str_compare(aaa.aval, "Package"))      { col.i = CCI_PKG; }
                                        else if (str_compare(aaa.aval, "ParsedVAL"))    { col.i = CCI_PARSEDVAL; }
                                        else if (str_compare(aaa.aval, "DNF"))          { col.i = CCI_DNF; }
                                        else
                                        {
                                            cout << "* W: unknown column field: \"" << aaa.aval << "\"\n";
                                        }
                                    }
                                    else if (str_compare(aaa.aname, "custom"))
                                    {
                                        col.custom = aaa.aval;
                                        if (col.custom.empty())
                                        {
                                            cerr << "* Error: custom column empty.\n";
                                            return RC_ERROR_ELEMENT;
                                        }
                                    }
                                    else
                                    {
                                        logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem1.path, "\n");
                                    }
                                }
                                v_clrinfl.push_back(col);
                            }
                            else
                            {
                                logger("/// E: color_influence:col attributes are required (line: ",elem1.doc_line,").\n");
                                return RC_ERROR_NO_ATTR;
                            }
                        }
                        else
                        {
                            elem1.print_w_unhandled(cout);
                        }
                        res1 = elem1.go_down_and_read();
                        if (res1 == RC_NO_NEXT) { break; }
                    }
                }
                else
                {
                    cerr << "Error: CFG: bad color_influence.\n";
                    return res1;
                }
            }
            else
            {
                elem0.print_w_unhandled(cout);
            }

            res = elem0.go_down_and_read();
            if (res == RC_NO_NEXT) { break; }
        }
        // if color_influence wasn't specified - use the group_by settings
        if (v_clrinfl.empty()) { v_clrinfl = v_groupby; }
        if      (csv_delim.empty())     { csv_delim = ","; }
        else if (csv_delim == R"(\t)")  { csv_delim = "\t"; }
        return RC_SUCCESS;
    }

    bool parse_regex_file(std::istream &is)
    {
        // "(C|R)(_\\d*_\\d*Metric)_.+" with "$1$1"
        //cfg.v_fp2pkg_re.push_back("(C|R)(_\\d*_\\d*Metric)");
/*
s/(C|R|L)(_\d*)(_\d*Metric)|(_Pad\d*.\d*x\d*.\d*mm)(_HandSolder)/$1$2
s/(LED)(_\d*)(_\d*Metric)(_Castellated|_HandSolder)/$1$2
s/(.+)(_HandSoldering|_HandSolder)/$1
*/
        v_fp2pkg_re.clear();
        string line;
        while (std::getline(is, line))
        {
            // expecting "s/one/two"
            // or "# ..." comment
            if (line.find("#", 0) == 0) { continue; }
            line.erase(std::remove(line.begin(), line.end(), '\r'), line.end());
            line.erase(std::remove(line.begin(), line.end(), '\n'), line.end());
            re_instr_t re;
            size_t p0 = 0;
            size_t p1 = line.find("s/", p0);
            if (p1 == 0)
            {
                p0 += 2;
                p1 = line.find("/", p0);
                if (p1 != string::npos)
                {
                    re.aaa = line.substr(p0, p1-p0);
                    p0 = p1+1;
                    p1 = line.find("/", p0);
                    re.bbb = line.substr(p0, p1-p0);
                }
            }
            if (re.aaa.empty() || re.bbb.empty())
            {
                cerr << "* E: bad regex line: " << line << "\n";
            }
            else
            {
                //cout << "re_instr: \"" << re.aaa << "\" \"" << re.bbb << "\"\n";
                v_fp2pkg_re.push_back(re);
            }
        }
        return true;
    }
    bool gen_html(std::ostream& os, const vector<column_order_t> &v_co) const;

    string subpath;
    string pv_delim;
    string csv_delim;
    string ofn_fmt;
    string dnf_cfname;  //!< custom field name for Do-Not-Fit
    string dnf_val;     //!< field value for Do-Not-Fit
    string name;        //!< config name
    //vector<size_t> v_colorder; //!< column order
    vector<column_order_t> v_colorder;  //!< Column Order (in the output)
    vector<column_order_t> v_sortorder; //!< Sorting Order
    vector<column_order_t> v_groupby;   //!< Grouping By (which fields)
    vector<column_order_t> v_clrinfl;   //!< Color Influence
    vector<re_instr_t> v_fp2pkg_re; //!< regex for footprint-to-package
};

struct FIELD
{
    string name;    //!< like "Notes", this is only used after during parsing and processing
    size_t fidx;    //!< field index.. to get the name of the field from the BOM.v_fields
    string val;     //!< like "pls buy mounting screws!"
};

struct COMP
{
    COMP() : dnf(false), count(0), total(0) {}
    // component
    string ref;         //!< annotated reference, like "C27"
    string value;       //!< value or partial/full partname, like "10u/16"
    string footprint;   //!< "lib:footprint_name"
    string datasheet;   //!< this might be empty
    string lib;         //!< symbol library, like "Device"
    string part;        //!< symbol, like "C"
    string desc;        //!< symbol description from the library, like "Unpolarized capacitor"


    bool dnf;       //!< "do not fit" flag
    bool v8_dnp;    //!< DNP flag since kicad v8
    string refdes;  //!< ref without the annotated number, like "C"
    string fp;      //!< footprint without the library, like "C_0805_2012Metric"
    string fplib;   //!< footprint library
    string pkg;     //!< package name, if none - use a simplified version of the footprint name
    string rfv;     //!< refdes:footprint:value, usable for sorting purposes
    string pv;      //!< package:value, usable for BOM
    string parent;  //!< which BOM does this belong to?
    int count;      //!< count for 1 board
    int total;      //!< count after multiplication
    FIELD config;   //!< board config (as in kibom)
    // TODO: fractional part count? maybe via a custom field?
    string parsedval; //!< normalized+remainder
    string parsedval_nrm; //!< parsed value - normalized (47000)
    string parsedval_opt; //!< parsed value - optimized (47k)
    string parsedval_rem; //!< parsed value - remainder

    // custom fields
    vector<FIELD> m_field;
    //vector<string> v_sv; //!< use this for sorting (and merging?)
};


struct GROUP : protected vector<COMP*>
{
    // a group of components.. hm
    // this should point to at least one or more COMPonents
    // this would appear as a single position/row in the BOM/table

    // this should have helper methods for boring tasks like
    // - get_count() which sums up the counts of all components
    // - get_ref() which returns "C13" or "C2 C3 C15 C27 C36"
    // - get_value() which could use a std::set to get a list of unique values
    // - get_pv_html() which could use a std::set too and then return a fancy list
    //     of Package:Value with color'ed <span> elements

    // the BOM struct should have a vector<GROUP>
    // sorting and grouping operations should be done on the GROUPs, not the COMPs
    // the source BOMs shall be kept around when merging into a final BOM
    // m_comp should not be fiddled with, to keep the pointers valid

    //inline bool empty() { return v_c.empty(); }
    //inline size_t size() { return v_c.size(); }

    // expose some of the vector things to be public
    // this would expose the constructor.. vector<T> vec = { ... };
    //GROUP(initializer_list<COMP*> il) : vector<COMP*>(il) {}
    using vector<COMP*>::begin;
    using vector<COMP*>::end;
    using vector<COMP*>::size;
    using vector<COMP*>::empty;
    using vector<COMP*>::clear;
    void add(COMP &comp) /// add a component to the group
    {
        simple = false;
        push_back(&comp);
        if (size() == 1) { simple = true; }
    }
    COMP& operator()() { return *vector<COMP*>::at(0); }
    inline bool is_simple()
    {
        if (size() <= 1) { return true; }
        simple = false;
        const COMP &a = *this->at(0);
        size_t i = 1;
        bool same = true;
        while (i < size())
        {
            const COMP &b = *this->at(i);
            // which things are really important?
            same &= (a.dnf          == b.dnf);
            same &= (a.value        == b.value);
            same &= (a.footprint    == b.footprint);
            same &= (a.datasheet    == b.datasheet);
            same &= (a.part         == b.part);
            same &= (a.lib          == b.lib);
            same &= (a.desc         == b.desc);
            // if there ever is a mechanism that would allow two BOMs to be loaded/processed
            // with different configs and/or fp2pkg - then the auto-generated attributes like
            // pkg would have to be compared too
            same &= (a.pkg          == b.pkg);
            // TODO: custom fields?

            if (same != true) { break; }
            ++i;
        }
        if (same) { simple = true; }
        return simple;
        //if (size() == 1) { return true; }
        // if all (or all important) properties are equal in the whole group
        // this should maybe be precomputed
        //return false;
    }
    void mult(const int n)
    {
        for (auto &c : *this)
        {
            c->total = (c->count * n);
        }
    }
    int get_count() { int v = 0; for (auto &c : *this) { v += c->count; } return v; }
    int get_total() { int v = 0; for (auto &c : *this) { v += c->total; } return v; }
    string get_ref(const string delim = " ")
    {
        string s;
        bool first = true;
        for (COMP *c : *this)
        {
            if (first == false) { s += delim; }
            first = false;
            s += c->ref;
        }
        return s;
    }
    string get_pv_html();
    string get_symb_html();
    string get_desc_html();
    string get_srcbom(const string delim = " ")
    {
        std::set<string> lut;
        for (COMP *c : *this) { lut.insert(c->parent); }
        vector<int> v_c;
        for (const string &x : lut)
        {
            // find all components with this parent, get their count
            int count = 0;
            for (COMP *c : *this)
            {
                if (str_compare(x, c->parent)) { count += c->count; }
            }
            v_c.push_back(count);
        }
        string s;
        bool first = true;
        size_t k = 0;
        for (const string &x : lut)
        {
            if (first == false) { s += delim; }
            first = false;
            s += x + '(' + std::to_string(v_c[k]) + ')';
            ++k;
        }
        return s;
    }

    std::set<string> get_refdes()
    {
        std::set<string> lut;
        for (COMP *c : *this) { lut.insert(c->refdes); }
        return lut;
    }
    std::set<string> get_datasheet()
    {
        std::set<string> lut;
        for (COMP *c : *this) { lut.insert(c->datasheet); }
        return lut;
    }
    std::set<string> get_field_val(const size_t fidx, bool &has_val)
    {
        has_val = false;
        std::set<string> lut;
        for (COMP *c : *this)
        {
            assert(fidx < c->m_field.size());
            FIELD &fld = c->m_field[fidx];
            //assert(fld.fidx == fidx); // fields which were auto-generated have fidx = -1
            if (fld.fidx == fidx) { has_val = true; }
            lut.insert(fld.val);
        }
        return lut;
    }

    //void mk_sortable(const CFG &cfg);
    vector<std::string_view> v_sv; //!< this is used for sorting

    protected:
    bool simple;
    //vector<COMP*> v_c; //!< vector of pointers to components
};

struct namenlib_t
{
    string name;
    string lib;
};
struct pkg_fp_lib_t
{
    string pkg;
    string fp;
    string lib;
};
struct pv_t : public pkg_fp_lib_t
{
    string val;
};
struct parsedval_t
{
    string value;       //!< original component value (like "0.1uF/50V")
    string parsedval;   //!< parsed, normalized value (like "0.0000001F/50V") suitable for sorting
    string optimized;   //!< parsed, optimized value (like "100n")
    string remainder;   //!< remainder from the parsed value (like "F/50V")
};
inline bool operator< (const namenlib_t &lhs, const namenlib_t &rhs)
{
    if (lhs.name < rhs.name) { return true; }
    if (lhs.lib  < rhs.lib)  { return true; }
    return false;
}
inline bool operator< (const pkg_fp_lib_t &lhs, const pkg_fp_lib_t &rhs)
{
    if (lhs.fp  < rhs.fp)  { return true; }
    if (lhs.pkg < rhs.pkg) { return true; }
    if (lhs.lib < rhs.lib) { return true; }
    return false;
}
inline bool operator< (const pv_t &lhs, const pv_t &rhs)
{
    if (lhs.val < rhs.val) { return true; }
    //if (lhs::pkg_fp_lib_t < rhs::pkg_fp_lib_t) { return true; }
    #if 1
    if (lhs.fp  < rhs.fp)  { return true; }
    if (lhs.pkg < rhs.pkg) { return true; }
    if (lhs.lib < rhs.lib) { return true; }
    #endif // 0
    return false;
}
inline bool operator< (const parsedval_t &lhs, const parsedval_t &rhs)
{
    if (lhs.value     < rhs.value)     { return true; }
    if (lhs.parsedval < rhs.parsedval) { return true; }
    if (lhs.optimized < rhs.optimized) { return true; }
    if (lhs.remainder < rhs.remainder) { return true; }
    return false;
}
struct tbl_group_t
{
    vector<string> ref;
    vector<pv_t> pv;
    vector<pkg_fp_lib_t> pkg;
    //vector<string> val;
    vector<parsedval_t> pval;
    vector<namenlib_t> fp;
    vector<namenlib_t> sym;
    vector<string> desc;
    vector<string> datash;
    //vector<string> srcbom;
    uint32_t csum32;
    float color;
    float clry; //!< color Y
    float clrc; //!< color chroma
};

struct STATS
{
    // BOM statistics
    STATS() : hack(false) {}
    bool gen_html(std::ostream& os);

    bool hack;
    int n_board_count;
    int n_unique_components;
    int n_components;
    string xml_name;
    string source;  //!< absolute filepath to the .sch
    string date;    //!< fuggly formatted date
    string tool;    //!< eeschema name and version

    string title;
    string rev;
    string date2;
    string company;
    vector<string> comments;
    // helper members
    string short_source;    //!< shorter version of source.sch
    string title2;          //!< either the title or the source.sch
};

struct BOM
{
    BOM() : pcfg(nullptr) {}
    void set_cfg(CFG &cfg) { pcfg = &cfg; }
    PARSER_RETCODE parse_comp(const element_t& root_elem);
    PARSER_RETCODE process(const string xml_filename);
    bool has_customfield(const string &name)
    {
        size_t i = 0;
        while (i < v_field_names.size())
        {
            if (str_compare(name, v_field_names[i])) { return true; }
            ++i;
        }
        return false;
    }
    const FIELD *get_field(const COMP &c, const string &name) /// for custom fields only! nullptr on fail
    {
        // field.fidx is the index to v_field_names plus CO__N bias
        if (c.m_field.size() > 0)
        {
            size_t i = 0;
            while (i < v_field_names.size())
            {
                if (str_compare(name, v_field_names[i]))
                {
                    // find a field with fidx = i+bias
                    const size_t fidx = (i + CO__N);
                    size_t k = 0;
                    while (k < c.m_field.size())
                    {
                        const FIELD &fld = c.m_field[k];
                        if (fld.fidx == fidx) { return &fld; }
                        ++k;
                    }
                    return nullptr;
                }
                ++i;
            }
        }
        return nullptr;
    }
    size_t lu_field(const string &name) /// get the custom field index
    {
        size_t fidx = 0;
        while (fidx < v_field_names.size())
        {
            if (str_compare(name, v_field_names[fidx])) { return fidx; }
            ++fidx;
        }
        throw std::logic_error("The requested custom field was not found.");
        return -1;
    }
    bool process2();
    bool process3();
    bool group2();
    bool can_group(const COMP &a, const COMP &b);
    void mk_sortable_g(GROUP &g);
    bool restore_fields(); /// call this before calling process3() "again"
    //bool process_fields();
    bool sort();
    //bool merge();
    bool group();
    bool merge_bom(vector<BOM> &v);
    bool mult(const int n);
    //bool mk_sortable_vec();
    bool calc_stats();
    bool gen_html(std::ostream& os);
    bool gen_csv( std::ostream& os);
    tbl_group_t prep(GROUP &g);

    string name;    //!< filename of the input .xml pls
    string aofn;     //!< synthesized output filename (from cfg format) .. for single BOM export, available after calling process()
    int num_boards; //!< passed from commandline arguments, pls no zero here

    // the following are part of the <design> element
    string source;  //!< absolute filepath to the .sch
    string date;    //!< fuggly formatted date
    string tool;    //!< eeschema name and version
    string tool_ver; //!< version (if detected)
    int tool_ver1;   //!< version major
    void set_tool_version(std::string_view what);
    // seems the revision and date are part of the <sheet> element (not directly)
    // and there can be multiple <sheet> elements, so.. maybe grab the root sheet?
    // them fields are optional so they may be empty too
    string sch_name;                //!< root sheet filename
    string sch_rev;                 //!< root sheet revision
    string sch_date;                //!< root sheet date
    string sch_title;               //!< root sheet title
    string sch_company;             //!< root sheet company
    vector<string> sch_comments;    //!< root sheet comments

    vector<COMP>    m_comp;
    vector<GROUP>   v_group;
    std::set<string>    m_ufields; //!< list of the unique fields in the BOM
    vector<string>      v_field_names;
    vector<STATS>   v_stats;
    CFG *pcfg;
};

void BOM::set_tool_version(std::string_view what)
{
    tool_ver = what;
    tool_ver1 = -1; // invalid/unknown
    size_t i = 0;
    const size_t n = what.size();
    int tmp = 0;
    size_t ndots = 0;
    size_t k = 0;
    while (i < n)
    {
        char c = what[i];
        if ((c >= '0') && (c <= '9'))
        {
            tmp = 10*tmp + (c - '0');
            ++k;
        }
        else if (c == '.')
        {
            if ((ndots == 0) && (k > 0))
            {
                tool_ver1 = tmp;
            }
            k = 0;
            tmp = 0;
            ++ndots;
        }
        else
        {
            break;
        }
        ++i;
    }
    if (ndots != 2) { tool_ver1 = -1; }
}

struct BOM_PARSER
{
    PARSER_RETCODE parse(const XMLElement *root);
    PARSER_RETCODE parsenprint(const XMLElement *root, const size_t start_level = 0);
    //bool memb_check_sizes(vector<member_ptr_t> &mpv);
    bool process();
    bool gen(std::ostream& os);

    // ----------------
    element_t       elem0;
    string              src_fn; //!< filename of the input XML file..
    vector<BOM>         v_bom;

};

PARSER_RETCODE BOM_PARSER::parse(const XMLElement *root)
{
    logger("Parsing...\n");
    if (root == nullptr) { return RC_ERROR_NULLPTR; }
    PARSER_RETCODE res = RC_SUCCESS;
    element_t root_elem;
    element_t elem1;
    if (!root_elem.read(root)) { return RC_ERROR_NULLPTR; } // TODO: fix this error code
    elem0 = root_elem;
    res = elem0.go_in_and_read(nullptr, "export");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_ELEMENT; }
    elem0.print(logger.of);

    //cout << e->FirstChild().Value() << "\n";
    //const XMLElement *e_periphs = nullptr;

    BOM bom;
    bom.name = src_fn;
    bom.tool_ver1 = -1;
    {
        // scan level 0
        //const XMLElement *e0 = root->FirstChildElement();
        //while (e0 != nullptr)
        while (true)
        {
            //if (!svde.read(e0)) { return RC_ERROR_SVD_ELEMENT; }
            elem0.print(logger.of);
            fumber64_t tmp;

            //cout << ">> " << elem0.ename << "\n";
            string sss;
            if (elem0.get("design"))
            {
                /*  Define the number of data bits uniquely selected by each address.
                 *  The value for Cortex-M-based devices is 8 (byte-addressable).
                 */
                 //if (sss.empty()) { logger("* Warning: device:addressUnitBits expected 8, got: ", tmp.str(), "\n"); }
                 //cout << ""
                 elem1 = elem0; res = elem1.go_in_and_read(nullptr, "dsgn");
                 if (res == RC_SUCCESS)
                 {
                    // looking for source, date, tool
                    while (true)
                    {
                        elem1.print(logger.of);

                        if      (elem1.get("source", bom.source)) {}
                        else if (elem1.get("date",   bom.date)) {}
                        else if (elem1.get("tool",   bom.tool))
                        {
                            constexpr std::string_view what = "Eeschema ";
                            if (bom.tool.starts_with(what))
                            {
                                std::string_view rem = bom.tool;
                                rem.remove_prefix(what.size());
                                if (!rem.empty())
                                {
                                    bom.set_tool_version(rem);
                                    if (bom.tool_ver1 != -1) { logger("Detected tool version: ", bom.tool_ver1, '\n'); }
                                    else { logger("/// W: couldn't detect tool version: ", bom.tool, '\n'); }
                                }
                            }
                        }
                        else if (elem1.get("sheet"))
                        {
                            //cout << "..sheet\n";
                            // TODO: try to get the root sheet
                            if (elem1.has_attr == false)
                            {
                                return RC_ERROR_NO_ATTR;
                            }
                            bool is_root_sheet = false;
                            for (const auto &aaa : elem1.v_attr)
                            {
                                if      (str_compare(aaa.aname, "number")) {}
                                else if (str_compare(aaa.aname, "name"))
                                {
                                    is_root_sheet = str_compare(aaa.aval, "/");
                                }
                                else if (str_compare(aaa.aname, "tstamps")) {}
                                else
                                {
                                    logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem1.path, "\n");
                                }
                            }
                            if (is_root_sheet)
                            {
                                PARSER_RETCODE res_tb0;
                                element_t elem_tb0 = elem1; res_tb0 = elem_tb0.go_in_and_read(nullptr, "sh");
                                if (res_tb0 == RC_SUCCESS)
                                {
                                    while (true)
                                    {
                                        if (elem_tb0.get("title_block"))
                                        {
                                            PARSER_RETCODE res_tb1;
                                            element_t elem_tb1 = elem_tb0; res_tb1 = elem_tb1.go_in_and_read(nullptr, "tb");
                                            if (res_tb1 == RC_SUCCESS)
                                            {
                                                while (true)
                                                {
                                                    if      (elem_tb1.get("title",  bom.sch_title)) {}
                                                    else if (elem_tb1.get("company",bom.sch_company)) {}
                                                    else if (elem_tb1.get("date",   bom.sch_date)) {}
                                                    else if (elem_tb1.get("rev",    bom.sch_rev)) {}
                                                    else if (elem_tb1.get("source", bom.sch_name)) {}
                                                    else if (elem_tb1.get("comment"))
                                                    {
                                                        for (const auto &aaa : elem_tb1.v_attr)
                                                        {
                                                            if      (str_compare(aaa.aname, "number")) {}
                                                            else if (str_compare(aaa.aname, "value"))
                                                            {
                                                                bom.sch_comments.push_back(aaa.aval);
                                                            }
                                                            else
                                                            {
                                                                logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem_tb1.path, "\n");
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        elem_tb1.print_w_unhandled(cout);
                                                    }
                                                    res_tb1 = elem_tb1.go_down_and_read();
                                                    if (res_tb1 == RC_NO_NEXT) { break; }
                                                }
                                            }
                                            else
                                            {
                                                return RC_ERROR_EMPTY_ELEMENT;
                                            }
                                        }
                                        res_tb0 = elem_tb0.go_down_and_read();
                                        if (res_tb0 == RC_NO_NEXT) { break; }
                                    }
                                }
                            }
                        }

                        res = elem1.go_down_and_read();
                        if (res == RC_NO_NEXT) { break; }
                    }
                    //cout << bom.source << " ::: " << bom.date << " ::: " << bom.tool << "\n";
                 }
            }
            #if 0
            else if (elem0.get("width",              tmp))
            {
                 if (tmp != 32) { logger("* Warning: device:width expected 32, got: ", tmp.str(), "\n"); }
            }
            //else if (svde.get("", regProp)) {}
            #endif // 0
            // skip these..
            else if (elem0.get("libparts"))  {}
            else if (elem0.get("libraries")) {}
            else if (elem0.get("nets"))      {}

            // this is wot we're looking for
            else if (elem0.get("components"))
            {
                //svde. e_periphs = e0;
                elem1 = elem0; res = elem1.go_in_and_read(nullptr, "cmp");
                if (res == RC_SUCCESS)
                {
                    // peripheral
                    elem0.print(logger.of);
                    while (true)
                    {
                        if (elem1.get("comp"))
                        {
                            //COMP comp;
                            //comp.count = 1;
                            PARSER_RETCODE res1 = bom.parse_comp(elem1);
                            if (res1 != RC_SUCCESS)
                            {
                                cerr << "* Error(" << (unsigned)(res1)
                                << ") while parsing component ("
                                << elem1.ename << " at line " << elem1.doc_line << ").\n";
                                return res1;
                            }
                        }
                        res = elem1.go_down_and_read();
                        if (res == RC_NO_NEXT) { break; }
                    }
                }
                else { return RC_ERROR_EMPTY_ELEMENT; }
            }
            else
            {
                elem0.print_w_unhandled(cout);
                //cout << "Warning: unhandled element: " << svde.ename << "\n";
            }
            res = elem0.go_down_and_read();
            if (res == RC_NO_NEXT) { break; }
            //e0 = e0->NextSiblingElement();
        }
        //logger("sz ", regProp.size.str(), " resetV ", regProp.resetValue.str(), "\n");
    }

    //const XMLElement *e_periphs = root->FirstChildElement("peripherals");
    //if (e_periphs == nullptr) { return RC_ERROR_PERIPHERALS; }
    //const XMLElement *e = e_periphs->FirstChildElement();
    //if (e == nullptr) { return RC_ERROR_EMPTY_PERIPHERALS; }

    //parsenprint(root, 0); return RC_SUCCESS;
    logger("  got ", bom.m_comp.size(), " components\n");
    v_bom.push_back(bom);
    return RC_SUCCESS;
}

PARSER_RETCODE BOM::parse_comp(const element_t& root_elem)
{
    element_t elem0 = root_elem;
    // the root element here is the "comp" which should have the ref as an attribute
    COMP comp;
    comp.count = 1;
    comp.v8_dnp = false;
    if (elem0.has_attr)
    {
        for (const auto &aaa : elem0.v_attr)
        {
            if (str_compare(aaa.aname, "ref"))
            {
                comp.ref = aaa.aval;
            }
            else
            {
                logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem0.path, "\n");
            }
        }
    }
    else
    {
        logger("/// E: component ref attributes are required (line: ",elem0.doc_line,").\n");
        return RC_ERROR_NO_ATTR;
    }

    PARSER_RETCODE res = elem0.go_in_and_read(nullptr, "cmp:");
    if (res == RC_NO_CHILDREN) { return RC_ERROR_EMPTY_ELEMENT; }
    //svde.print(logger.of);

    while (true)
    {
        elem0.print(logger.of);
        if      (elem0.get("value",          comp.value))        {}
        else if (elem0.get("footprint",      comp.footprint))    {}
        else if (elem0.get("datasheet",      comp.datasheet))    {}
        // v8 also began writing <description> this just silences the warning
        else if (elem0.get("description",    comp.desc))         {}

        // skip these
        else if (elem0.get("sheetpath")) {}
        else if (elem0.get("tstamp"))    {}
        else if (elem0.get("tstamps"))   {}
        else if (elem0.get("property"))
        {
            if (tool_ver1 != -1)
            {
                if (tool_ver1 >= 8)
                {
                    // look for properties with only a name: "dnp"
                    if (elem0.has_attr)
                    {
                        if (elem0.v_attr.size() == 1)
                        {
                            const auto &aaa = elem0.v_attr[0];
                            if (str_compare(aaa.aname, "name"))
                            {
                                if (aaa.aval == "dnp") { comp.v8_dnp = true; }
                            }
                        }
                    }
                }
            }
        }

        else if (elem0.get("libsource"))
        {
            // get attributes: lib, part, description
            if (elem0.has_attr)
            {
                for (const auto &aaa : elem0.v_attr)
                {
                    if      (str_compare(aaa.aname, "lib"))         { comp.lib  = aaa.aval; }
                    else if (str_compare(aaa.aname, "part"))        { comp.part = aaa.aval; }
                    else if (str_compare(aaa.aname, "description")) { comp.desc = aaa.aval; }
                    else
                    {
                        logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem0.path, "\n");
                    }
                }
            }
            else
            {
                logger("/// E: libsource attributes are required (line: ",elem0.doc_line,").\n");
                return RC_ERROR_NO_ATTR;
            }
        }
        else if (elem0.get("fields"))
        {
            // get the fields
            element_t elem1 = elem0; res = elem1.go_in_and_read(nullptr, "fld");
            if (res == RC_SUCCESS)
            {
                // looking for source, date, tool
                while (true)
                {
                    elem1.print(logger.of);

                    if (elem1.get("field"))
                    {
                        if (elem1.has_attr)
                        {
                            for (const auto &aaa : elem1.v_attr)
                            {
                                if (str_compare(aaa.aname, "name"))
                                {
                                    //comp.ref = aaa.aval;
                                    if (str_compare_nocs(aaa.aval, g_config_field))
                                    {
                                        // special field
                                        // TODO: this should maybe go away..
                                        comp.config.name = aaa.aval;
                                        comp.config.val = elem1.etext;
                                        //if (str_compare_nocs(comp.config.val, "dnf")) { cout << "Found DNF!\n"; }
                                    }
                                    else
                                    {
                                        // just a custom field, write it down
                                        FIELD fld;
                                        fld.fidx = 0; // invalid, use the .name, or optimize this at BOM level
                                        fld.name = aaa.aval;
                                        fld.val = elem1.etext;
                                        #if 0
                                        #warning "NO-FIELDS SIMULATOR IS ENABLED!"
                                        #else
                                        bool doit = true;
                                        if ((tool_ver1 != -1) && (tool_ver1 >= 8))
                                        {
                                            // since v8, these 3 builtin fields are also duplicated
                                            // and disguised as custom fields - skip them!
                                            // https://gitlab.com/kicad/code/kicad/-/issues/18793
                                            if      (fld.name == "Footprint")    { doit = false; }
                                            else if (fld.name == "Datasheet")    { doit = false; }
                                            else if (fld.name == "Description")  { doit = false; }
                                        }
                                        if (doit) { comp.m_field.push_back(fld); }
                                        #endif // 0
                                        //cout << "found custom field! " << fld.name << " = " << fld.val << "\n";
                                    }
                                }
                                else
                                {
                                    logger("/// W: unknown attribute: ", aaa.aname, "=", aaa.aval, " @ ", elem1.path, "\n");
                                }
                            }
                        }
                        else
                        {
                            logger("/// E: field name attribute is required (line: ",elem1.doc_line,").\n");
                            return RC_ERROR_NO_ATTR;
                        }
                    }
                    else
                    {
                        elem1.print_w_unhandled(cout);
                    }

                    res = elem1.go_down_and_read();
                    if (res == RC_NO_NEXT) { break; }
                }
            }
            else
            {
                cout << "* W: no fields?\n";
                return res;
            }
        }
        else
        {
            elem0.print_w_unhandled(cout);
        }

        res = elem0.go_down_and_read();
        if (res == RC_NO_NEXT) { break; }
    }

    if (comp.ref.empty())
    {
        cerr << "* Error: component without Reference.\n";
        return RC_ERROR_INSUFFICIENT_DATA;
    }
    if (comp.footprint.empty())
    {
        cerr << "* Error: component without Footprint.\n";
        return RC_ERROR_INSUFFICIENT_DATA;
    }

    //logger("*** ", comp.ref," ",comp.footprint," ",comp.value,"\n");
    m_comp.push_back(comp);
    return RC_SUCCESS;
}

constexpr char g_pv_delim[] = ":";

string fp2pkg_regex(const string fp, const vector<re_instr_t> v_what)
{
    try
    {
        //cout << "Trying: " << rrr << "\n";
        for (const re_instr_t &ins : v_what)
        {

            std::regex re(ins.aaa);
            //std::smatch m;
            //cout << regex_search(fp, m, re) << "\t";
            //cout << m.str() << "\n";
            //if (regex_search(fp, m, re)) { return m.str(); }
            string rslt = regex_replace(fp, re, ins.bbb);
            if (rslt != fp)
            {
                return rslt;
            }
        }
    }
    catch (std::regex_error& e)
    {
        cerr << "Regex error: " << e.what() << "\n";
    }
    return fp;
}
size_t fp2pkg_regex_alt(const re_instr_t ins, vector<COMP> &v_c)
{
    size_t j = 0;
    try
    {
        std::regex re(ins.aaa);
        string rslt;
        for (COMP &comp : v_c)
        {
            if (comp.pkg.empty())
            {
                rslt = regex_replace(comp.fp, re, ins.bbb);
                if ((rslt != comp.fp) && (!rslt.empty()))
                {
                    comp.pkg = rslt;
                    ++j;
                }
                rslt.clear();
            }
        }
    }
    catch (std::regex_error& e)
    {
        cerr << "Regex error: " << e.what() << "\n";
    }
    return j;
}

PARSER_RETCODE BOM::process(const string xml_filename)
{
    logger("process()\n");
    if (pcfg == nullptr) { throw std::logic_error("pcfg is nullptr"); }
    const CFG &cfg = *pcfg;

    {
        sch_name = fs::path(source).filename().string();
    }

    assert(v_stats.empty() == true);
    // make a statistics object
    STATS stats;
    // fill some basic things into it
    {
        stats.xml_name  = xml_filename;
        stats.source    = source;
        stats.tool      = tool;
        stats.date      = date;
        // TODO: date and sch_date might be two different things!
        stats.date2     = sch_date;
        stats.rev       = sch_rev;
        stats.title     = sch_title;
        stats.company   = sch_company;
        stats.comments  = sch_comments;

        stats.n_board_count         = 0;
        stats.n_unique_components   = 0;
        stats.n_components          = 0;
        // ----------
        {
            stats.short_source = fs::path(stats.source).filename().string();
            // if there's no title, use the short version of the source.sch
            if (stats.title.empty())    { stats.title2 = stats.short_source; }
            else                        { stats.title2 = stats.title; }

        }
    }
    std::set<string> ufields;
    // scan all components
    size_t k = 0;
    v_group.clear();
    v_group.reserve(m_comp.size());
    while (k < m_comp.size())
    {
        COMP &comp = m_comp[k];

        // generate refdes (ref minus the annotation)
        {
            comp.refdes.clear();
            size_t i = 0;
            while (i < comp.ref.size())
            {
                const char c = comp.ref[i];
                if ((c >= '0') && (c <= '9')) { break; }
                comp.refdes += c;
                ++i;
            }
        }
        // generate fp (footprint minus the library)
        {
            comp.fp = comp.footprint;
            const size_t p0 = comp.footprint.find(':');
            if (p0 != string::npos)
            {
                comp.fp = comp.footprint.substr(p0+1);
                comp.fplib = comp.footprint.substr(0, p0);
            }
            else
            {
                comp.fp = comp.footprint;
                comp.fplib = "???";
                cout << "* W: footprint doesn't contain library? (" << comp.footprint << ")\n";
            }
        }
        ++k;
    }
    #define FP2PKG_ALT
    #ifndef FP2PKG_ALT
    for (COMP &comp : m_comp)
    {
        // generate package (or simplified footprint name, minus the library)
        {
            if (comp.pkg.empty())
            {
                // TODO: provide a way to search&replace the footprint name to simplify it
                //comp.pkg = comp.fp;
                comp.pkg = fp2pkg_regex(comp.fp, cfg.v_fp2pkg_re);
            }
        }
    }
    #else
    {
        size_t nre = 0;
        for (const re_instr_t &rei : cfg.v_fp2pkg_re)
        {
            nre += fp2pkg_regex_alt(rei, m_comp);
            // hm what if there are no matches? or no instructions in the config?
        }
        if (nre > 0) { cout << "fp2pkg(): " << nre << " matches.\n"; }
    }
    // after all of that, if pkg is still empty - assign the footprint name *shrug*
    for (COMP &comp : m_comp)
    {
        if (comp.pkg.empty()) { comp.pkg = comp.fp; }
    }
    #endif // FP2PKG_ALT
    for (COMP &comp : m_comp)
    {
        // generate package:value
        // generate refdes:footprint:value
        // this is maybe no longer useful
        {
            const auto &d = g_pv_delim;
            comp.pv     = comp.pkg      + d + comp.value;
            comp.rfv    = comp.refdes   + d + comp.fp       + d + comp.value;
        }
        #if 0
        // record the unique fields used in the whole BOM
        // any "special ones" should have been removed before here maybe?
        {
            size_t j = 0;
            while (j < comp.m_field.size())
            {
                //m_ufields.insert(comp.m_field[j].name);
                ufields.insert(comp.m_field[j].name);
                ++j;
            }
        }
        #endif // 0

        // deal with the Do-Not-Fit stuff
        comp.dnf = false;
        if (!cfg.dnf_cfname.empty())
        {
            for (FIELD &fld : comp.m_field)
            {
                if (str_compare(fld.name, cfg.dnf_cfname))
                {
                    bool used = true;
                    // maybe if the config has no value for DNF - any text counts as DNF?
                    if ((cfg.dnf_val.empty()) && (fld.val.empty() == false)) { used = false; }
                    else
                    {
                        if (str_compare(fld.val, cfg.dnf_val)) { used = false; }
                    }

                    if (used == false)
                    {
                        comp.dnf = true;
                    }
                }
            }
        }
        if (comp.v8_dnp) { comp.dnf = true; }
        if (comp.dnf)
        {
            // set the count to zero?
            comp.count = 0;
        }
    }
    // generate parsed value
    {
        PassiveValueParser psv;
        enum passive_comp_t
        {
            resistor = 0,
            capacitor,
            inductor,
            crystal,
            unknown
        };
        vector<string> units_r = { "R", "Ω" };
        vector<string> units_c = { "F" };
        vector<string> units_l = { "H" };
        vector<string> units_x = { "Hz" };
        vector<string> *units[5] =
        {
            &units_r,
            &units_c,
            &units_l,
            &units_x,
            nullptr
        };
        for (COMP & comp : m_comp)
        {
            // Detect Passive components based on RefDes
            passive_comp_t pct = unknown;
            if      (comp.refdes == "R") { pct = resistor; }
            else if (comp.refdes == "C") { pct = capacitor; }
            else if (comp.refdes == "L") { pct = inductor; }
            else if (comp.refdes == "Y") { pct = crystal; }
            else if (comp.refdes == "RV") { pct = resistor; }
            else if (comp.refdes == "RN") { pct = resistor; }
            else if (comp.refdes == "RT") { pct = resistor; }
            else if (comp.refdes == "FB") { pct = inductor; }

            if (psv.parse(comp.value, units[pct]))
            {
                psv.proc();
                //comp.parsedval2 = psv._val2;
                comp.parsedval = psv.normalized + psv.remainder;
                comp.parsedval_nrm = psv.normalized;
                comp.parsedval_opt = psv.optimized;
                comp.parsedval_rem = psv.remainder;
            }
        }
    }

    {
        string xmlname;
        {
            name = xml_filename;
            {
                fs::path p(name);
                //xmlname = p.stem(); // doesn't wurk on mingw?
                xmlname = p.stem().string();
            }
            k = 0;
            while (k < m_comp.size())
            {
                COMP &comp = m_comp[k];
                // add BOM name to components
                {
                    // the count can be put afterwards
                    comp.parent = xmlname; // + '(' + std::to_string(comp.count) + ')';
                }
                ++k;
            }
        }
        {
            aofn = cfg.ofn_fmt;
            string nowdate;
            {
                std::time_t t = std::time(nullptr);
                std::tm tm = *std::localtime(&t);
                std::ostringstream oss;
                oss << std::put_time(&tm, "%Y-%m-%d"); // %F seems to return nothing on mingw
                nowdate = oss.str();
            }
            string zschname = fs::path(sch_name).stem().string();
            #if 0
            cout << "%n: " << xmlname << "\n";
            cout << "%s: " << zschname << "\n";
            cout << "%R: " << sch_rev << "\n";
            cout << "%D: " << nowdate << "\n";
            #endif // 0
            str_replace(aofn, "%n", xmlname);
            str_replace(aofn, "%s", zschname);
            str_replace(aofn, "%R", sch_rev);
            str_replace(aofn, "%D", nowdate);
            //cout << "Synthesized output filename: " << aofn << "\n";
        }
    }
    v_stats.push_back(stats);
    return RC_SUCCESS;
}

bool BOM::process2()
{
    logger("process2()\n");
    /*
        process() should be called after parsing individual BOM
        it fixes some basic things up


        process2() initializes GROUPs
        puts each COMP into a GROUP

        process3() deals with the fields

        mult() multiplies component counts by N

        sort()

        group2()


        sorting and merging can be done on the GROUPs
        bonus points if sorting can be done both before and after group2()

    */
    /*
        custom fields are a problem:
        they gotta be pre-processed after parsing, then made consistent
        they gotta be pre-processed before merging BOMs too
        merging BOMs could be done on the GROUPs instead
        hm..

    */
    v_group.clear();
    v_group.reserve(m_comp.size());
    for (auto &c : m_comp)
    {
        GROUP g;
        g.add(c);
        v_group.push_back(g);
    }

    return true;
}

bool BOM::process3()
{
    // process the custom fields, make them consistent
    // destructive! this clears the field.name
    // TODO resolve this.. process3() must be called for a 2nd time when merging BOMs

    logger("process3()\n");
    {
        // record all unique field names
        std::set<string> ufields;
        bool has_fields = false;
        for (GROUP &g : v_group)
        {
            for (COMP *comp : g)
            {
                COMP &c = *comp;
                for (FIELD &fld : c.m_field)
                {
                    ufields.insert(fld.name);
                    has_fields = true;
                }
            }
        }
        // now put the unique fields into v_field_names
        v_field_names.clear();
        if (has_fields)
        {
            for (const auto &ufld : ufields)
            {
                v_field_names.push_back(ufld);
            }
            cout << "- BOM has " << v_field_names.size() << " unique fields.\n";
            for (const auto &fld : v_field_names) { cout << "--- " << fld << "\n"; }
        }
        // lu_fields() should be usable now
    }
    {
        // make the fields consistent
        // use the order that's in the v_field_names vector
        // if a component doesn't have some of the fields - generate them
        for (GROUP &g : v_group)
        {
            for (COMP *comp : g)
            {
                COMP &c = *comp;
                vector<FIELD> tmp;
                {
                    FIELD emptyfld; emptyfld.fidx = -1;
                    tmp.reserve(v_field_names.size());
                    tmp.resize( v_field_names.size(), emptyfld);
                }
                for (FIELD &fld : c.m_field)
                {
                    // find the fidx of this
                    const size_t fidx = lu_field(fld.name);
                    assert(fidx < tmp.size());
                    FIELD &fld2 = tmp[fidx];
                    fld2.val = fld.val;
                    fld2.fidx = fidx;
                }
                tmp.swap(c.m_field);
                assert(c.m_field.size() == v_field_names.size());
            }
        }
    }
    return true;
}
bool BOM::restore_fields()
{
    // process3() "optimizes" the fields, but clears their names
    // when you want to merge multiple BOMs, and process3() has
    // been called on each of them once, then this must be called before
    // the merging! .. perhaps it should be automatically called from
    // merge_bom()
    for (GROUP &g : v_group)
    {
        for (COMP *comp : g)
        {
            COMP &c = *comp;
            assert(c.m_field.size() == v_field_names.size());
            size_t k = 0;
            for (const string &x : v_field_names)
            {
                c.m_field[k].name = x;
                ++k;
            }
        }
    }
    return true;
}

bool BOM::group2()
{
    if (pcfg == nullptr) { throw std::logic_error("pcfg is nullptr"); }
    //const CFG &cfg = *pcfg;
    //const vector<column_order_t> &v_gb = cfg.v_groupby;

    vector<GROUP> tmp;
    tmp.reserve(v_group.size());

    // if some of the GROUPs already contain more than 1 component things get difficult
    {
        bool simple = true;
        size_t n = 0;
        for (GROUP &g : v_group)
        {
            n += g.size();
            if (g.size() > 1) { simple = false; }
            else if (g.size() != 1) { throw std::logic_error("empty GROUP"); }
        }
        if (simple == false)
        {
            logger("Ungrouping..\n");
            tmp.reserve(n);
            for (GROUP &g : v_group)
            {
                for (COMP *comp : g)
                {
                    GROUP group;
                    group.add(*comp);
                    tmp.push_back(group);
                }
            }
            tmp.swap(v_group);
            tmp.clear();
            tmp.reserve(v_group.size());
        }
    }

    // up to here there should be only 1 COMP in each GROUP
    // - clone v_group into tmp
    //
    //vector<size_t> lut(v_group.size());
    {
        assert(tmp.size() == 0);
        assert(v_group.size() > 0);
        tmp = v_group;
        assert(tmp.size() == v_group.size());
        size_t k = 0;
        while (k < tmp.size())
        {
            GROUP &g0 = tmp[k];
            if (g0.size() > 0)
            {
                //assert(g0.size() == 1);
                COMP &a = g0();
                // scan all the following groups
                size_t i = k+1;
                while (i < tmp.size())
                {
                    GROUP &g1 = tmp[i];
                    if (g1.size() != 0)
                    {
                        assert(g1.size() == 1);
                        COMP &b = g1();
                        // compare a and b
                        if (can_group(a, b))
                        {
                            // add component B to the group where A is
                            // remove B from its original group (clear the group)
                            g0.add(b);
                            g1.clear();
                        }
                        //else { if (str_compare(b.refdes, "BZ")) { cout << "can_group():" << a.value << " != " << b.value << " ::: " << (int)(a.dnf) << " " << (int)(b.dnf) << "\n"; } }
                    }
                    ++i;
                }
            }
            ++k;
        }
        // now count how many groups in tmp actually have components
        k = 0;
        for (GROUP &g : tmp) { if (!g.empty()) { ++k; } }
        // clear the original vector, push_back those groups
        v_group.clear();
        v_group.reserve(k);
        for (GROUP &g : tmp) { if (!g.empty()) { v_group.push_back(g); } }
        tmp.clear();
    }
    return true;
}

bool BOM::can_group(const COMP &a, const COMP &b)
{
    bool same = true;
    //size_t k = 0;
    const CFG &cfg = *pcfg;
    const vector<column_order_t> &v_gb = cfg.v_groupby;
    for (const column_order_t &cgb : v_gb)
    {
        if (cgb.used == false) { continue; }
        switch (cgb.i)
        {
            case CGB_REFDES     : { same &= (str_compare(a.refdes,      b.refdes)); } break;
            case CGB_FOOTPRINT  : { same &= (str_compare(a.fp,          b.fp)); } break;
            case CGB_FPLIB      : { same &= (str_compare(a.fplib,       b.fplib)); } break;
            case CGB_SYMBOL     : { same &= (str_compare(a.part,        b.part)); } break;
            case CGB_SYMLIB     : { same &= (str_compare(a.lib,         b.lib)); } break;
            case CGB_DESC       : { same &= (str_compare(a.desc,        b.desc)); } break;
            case CGB_VALUE      : { same &= (str_compare(a.value,       b.value)); } break;
            case CGB_DATASHEET  : { same &= (str_compare(a.datasheet,   b.datasheet)); } break;
            case CGB_PKG        : { same &= (str_compare(a.pkg,         b.pkg)); } break;
            case CGB_PARSEDVAL  : { same &= (str_compare(a.parsedval,   b.parsedval)); } break;
            case CGB_DNF        : { same &= (            a.dnf ==       b.dnf); } break;
            default:
            {
                if (cgb.i >= CGB__N)
                {
                    // custom field
                    const FIELD *flda = get_field(a, cgb.custom);
                    const FIELD *fldb = get_field(b, cgb.custom);
                    if ((flda != nullptr) && (fldb != nullptr))
                    {
                        same &= (str_compare(flda->val, fldb->val));
                    }
                    else if ((flda == nullptr) && (fldb == nullptr)) {}
                    else
                    {
                        throw std::logic_error("custom fields are inconsistent");
                        return false;
                    }
                }
                else
                {
                    throw std::logic_error("unknown grouping field.");
                }
            } break;
        };
        if (same == false) { break; }
    }
    return same;
}

bool compare_group(const GROUP &a, const GROUP &b)
{
    assert(a.v_sv.size() > 0);
    assert(b.v_sv.size() > 0);
    assert(a.v_sv.size() == b.v_sv.size());
    return (a.v_sv < b.v_sv);
}

/*
suggestion from ##C++-general:
{
    S   columns_left{"non-empty"s, ""s, "non-empty"s},
        columns_right{"non-empty"s, "non-empty"s, ""s};
    cout << (columns_left < columns_right);
}
struct S
{
    string col0, col1, col2;
};
bool operator<(S lhs, S rhs)
{
    return tie(lhs.col0, lhs.col1, lhs.col2) < tie(rhs.col0, rhs.col1, rhs.col2);
}
--------------
or using a vector of string_view, since you can then compare the whole vectors, funky

*/

bool BOM::sort()
{
    logger("Sorting ", v_group.size(), " groups.\n");
    for (GROUP &g : v_group) { mk_sortable_g(g); }

    std::sort(v_group.begin(), v_group.end(), compare_group);
    return true; // meh
}

bool BOM::mult(const int n)
{
    #if 0
    size_t k = 0;
    while (k < m_comp.size())
    {
        m_comp[k].total = n * m_comp[k].count;
        ++k;
    }
    #endif // 0
    assert(v_stats.size() == 1);
    v_stats[0].n_board_count = n;
    for (GROUP &g : v_group) { g.mult(n); }
    return true;
}
bool BOM::merge_bom(vector<BOM> &v)
{
    #if 0
    size_t k = 0;
    while (k < v.size())
    {
        BOM &b = v[k];
        vector<COMP> &va = m_comp;
        vector<COMP> &vb = b.m_comp;
        // v2.insert(v2.end(), std::make_move_iterator(v1.begin() + 7), std::make_move_iterator(v1.end()));
        va.insert(va.end(), std::make_move_iterator(vb.begin()), std::make_move_iterator(vb.end()));
        vb.erase(vb.begin(), vb.end());
        m_ufields.insert(b.m_ufields.begin(), b.m_ufields.end());
        ++k;
    }
    sort();
    merge();
    {
        cout << "Unique fields for the whole BOM:\n";
        for (const auto &uuu : m_ufields)
        {
            cout << "- " << uuu << "\n";
        }
    }
    #endif // 0
    assert(v_stats.empty() == true);
    {
        for (BOM &bom : v)
        {
            assert(bom.v_stats.size() > 0);
            {
                // these were added just to make the stats work.. they weren't needed otherwise
                // not sure what the side-effects would be
                bom.group2(); // hm..
                bom.calc_stats(); // hm..
            }
            for (auto &stats : bom.v_stats) { v_stats.push_back(stats); }
        }
    }
    assert(v_group.empty() == true);
    for (BOM &bom : v)
    {
        bom.restore_fields();
        for (auto &g : bom.v_group)
        {
            v_group.push_back(g);
        }
    }
    process3();
    group2();
    calc_stats();
    return true;
}

bool BOM::calc_stats()
{
    // this makes sense to be called only after group()
    // crap
    if (v_stats.size() == 0)
    {
        throw std::logic_error("calc_stats(): unexpected condition!");
    }
    size_t k = 0;
    if (v_stats.size() > 1)
    {
        // hack
        k = v_stats.size();
        v_stats.emplace_back();
        v_stats[k].hack = true;
    }
    //if (v_stats.size() == 1)
    //for (STATS &s : v_stats)

    STATS &s = v_stats[v_stats.size() - 1];
    {
        s.n_unique_components = v_group.size();
        s.n_components = 0;
        for (const GROUP &g : v_group) { s.n_components += g.size(); }
    }
    //else if (v_stats.size() > 1)
    //else

    if (s.hack)
    {
        s.n_board_count = 0;

        k = 0;
        while (k < (v_stats.size() - 1))
        {
            STATS &is = v_stats[k];
            s.n_board_count += is.n_board_count;
            ++k;
        }
    }
    return true;
}

template<bool nl = false>
struct scoped_tag
{
    scoped_tag(std::ostream &_os, const string _name, const string _attr = "") : os(_os)
    {
        name = _name;
        if (!_attr.empty())
        {
            if constexpr (nl)   { os << "<" << name << ' ' << _attr << ">\n"; }
            else                { os << "<" << name << ' ' << _attr << ">"; }
        }
        else
        {
            if constexpr (nl)   { os << "<" << name << ">\n"; }
            else                { os << "<" << name << ">"; }
        }
    }
    ~scoped_tag()
    {
        if constexpr (nl)   { os << "</" << name << ">\n"; }
        else                { os << "</" << name << ">"; }
    }

    string name;
    std::ostream &os;
};

void BOM::mk_sortable_g(GROUP &g)
{
    if (pcfg == nullptr) { throw std::logic_error("pcfg is nullptr"); }
    const CFG &cfg = *pcfg;
    const vector<column_order_t> &v_so = cfg.v_sortorder;
    g.v_sv.clear();
    size_t k = 0;
    for (const column_order_t &cso : v_so) { if (cso.used) { ++k; } }
    g.v_sv.reserve(k);

    // just get the properties from the first COMP in the group
    // coz.. meh
    // note: it may happen that the first COMP doesn't have a certain field
    // while the other COMPs may have it.. hm
    // * maybe sort the components in each group first, before sorting a whole vector of groups?
    if (g.is_simple() == false)
    {
        logger("W: Sorting non-simple group with ", g.size(), " components: ", g().value, "\n");
    }
    static string dnf_str[2] = { "0", "1" };
    COMP &c = g();
    for (const column_order_t &cso : v_so)
    {
        if (cso.used == false) { continue; }
        switch (cso.i)
        {
            case CSO_REF:       { g.v_sv.push_back(c.ref); }        break;
            case CSO_REFDES:    { g.v_sv.push_back(c.refdes); }     break;
            case CSO_PKG:       { g.v_sv.push_back(c.pkg); }        break;
            case CSO_VALUE:     { g.v_sv.push_back(c.value); }      break;
            case CSO_DESC:      { g.v_sv.push_back(c.desc); }       break;
            case CSO_DATASHEET: { g.v_sv.push_back(c.datasheet); }  break;
            case CSO_SYMBOL:    { g.v_sv.push_back(c.part); }       break;
            case CSO_PARSEDVAL: { g.v_sv.push_back(c.parsedval); }  break;
            case CSO_DNF:       { g.v_sv.push_back(dnf_str[c.dnf ? 1 : 0]); } break;
            default:
            {
                if (cso.i >= CSO__N)
                {
                    // custom field
                    // check if the BOM even has this custom field
                    if (has_customfield(cso.custom))
                    {
                        const size_t fidx = lu_field(cso.custom);
                        //cout << "mk_sortable_g() " << cso.custom << " fidx: " << fidx << "\n";
                        assert(fidx < c.m_field.size());
                        FIELD &fld = c.m_field[fidx];
                        //assert(fld.fidx == fidx); // auto-generated fields are set to -1
                        g.v_sv.push_back(fld.val);
                    }
                }
                else
                {
                    throw std::logic_error("unknown sorting field.");
                }
            } break;
        };
    }
    //for (const auto &sv : g.v_sv) { cout << ' ' << sv; } cout << "\n";
}

const char* g_html_newline = "&#013;"; //"&#13;";
const char* g_html_link_icon = "🔗";

string GROUP::get_pv_html()
{
    string s;
    std::set<vector<std::string>> ulut;
    for (COMP *comp : *this)
    {
        COMP &c = *comp;
        vector<std::string> sv = { c.pkg, c.value, c.fplib, c.fp };
        ulut.insert(sv);
    }

    bool first = true;
    for (auto &v : ulut)
    {
        //COMP &c = *comp;
        if (first == false) { s += "<br/>"; }
        first = false;

        s  += "<span class=\"bvp_clr\" title=\"Library: " + v[2] + g_html_newline + "Footprint: " + v[3] + "\">" + v[0] + "</span>";
        s  += "<span class=\"bvs_clr\">:</span>";
        s  += "<span class=\"bvv_clr\">" + v[1] + "</span>";
    }
    #if 1
    #else
    bool first = true;
    for (COMP *comp : *this)
    {
        COMP &c = *comp;
        if (first == false) { s += "<br/>"; }
        first = false;

        s  += "<span title=\"Library: " + c.fplib + g_html_newline
            + "Footprint: " + c.fp + "\">" + c.pkg + "</span>";
        s  += "<span style=\"color:grey\">:</span>";
        s  += "<span style=\"color:red\">" + c.value + "</span>";
    }
    #endif // 1
    return s;
}
string GROUP::get_symb_html()
{
    string s;
    std::set<vector<std::string>> ulut;
    for (COMP *comp : *this)
    {
        COMP &c = *comp;
        vector<std::string> sv = { c.part, c.lib };
        ulut.insert(sv);
    }

    bool first = true;
    for (auto &v : ulut)
    {
        //COMP &c = *comp;
        if (first == false) { s += "<br/>"; }
        first = false;
        s  += "<span title=\"Library: " + v[1] + "\">" + v[0] + "</span>";
    }
    return s;
}
string GROUP::get_desc_html()
{
    string s;
    std::set<string> ulut;
    for (COMP *comp : *this)
    {
        COMP &c = *comp;
        ulut.insert(c.desc);
    }

    bool first = true;
    for (auto &v : ulut)
    {
        //COMP &c = *comp;
        if (first == false) { s += "<hr/>"; }
        first = false;
        s  += "<span>" + v + "</span>";
    }
    return s;
}

tbl_group_t BOM::prep(GROUP &g)
{
    tbl_group_t t;
    {
        std::set<pv_t> lut_pv;
        std::set<pkg_fp_lib_t> lut_pkg;
        std::set<namenlib_t> lut_fp;
        std::set<namenlib_t> lut_sym;
        //std::set<string> lut_val;
        std::set<parsedval_t> lut_val;
        std::set<string> lut_datash;
        std::set<string> lut_desc;
        for (const COMP *comp : g)
        {
            //const COMP &c = *comp;
            {
                pv_t tpv;
                tpv.pkg = comp->pkg;
                tpv.val = comp->value;
                tpv.fp = comp->fp;
                tpv.lib = comp->fplib;
                lut_pv.insert(tpv);
            }
            {
                pkg_fp_lib_t tpf;
                tpf.pkg = comp->pkg;
                tpf.fp = comp->fp;
                tpf.lib = comp->fplib;
                lut_pkg.insert(tpf);
            }
            {
                namenlib_t tfp;
                tfp.name = comp->fp;
                tfp.lib = comp->fplib;
                lut_fp.insert(tfp);
            }
            {
                namenlib_t tsm;
                tsm.name = comp->part;
                tsm.lib = comp->lib;
                lut_sym.insert(tsm);
            }
            {
                parsedval_t tpv;
                tpv.value = comp->value;
                tpv.optimized = comp->parsedval_opt;
                tpv.parsedval = comp->parsedval;
                tpv.remainder = comp->parsedval_rem;
                lut_val.insert(tpv);
            }
            //lut_val.insert(comp->value);
            lut_datash.insert(comp->datasheet);
            lut_desc.insert(comp->desc);

            // ref goes in directly
            t.ref.push_back(comp->ref);
        }
        for (const pv_t &x          : lut_pv)   { t.pv.push_back(x); }
        for (const pkg_fp_lib_t &x  : lut_pkg)  { t.pkg.push_back(x); }
        for (const namenlib_t &x    : lut_fp)   { t.fp.push_back(x); }
        for (const namenlib_t &x    : lut_sym)  { t.sym.push_back(x); }
        //for (string &s  : lut_pkg)      { t.pkg.push_back(s); }
        for (const parsedval_t &s   : lut_val)  { t.pval.push_back(s); }
        for (const string &s    : lut_datash)   { t.datash.push_back(s); }
        for (const string &s    : lut_desc)     { t.desc.push_back(s); }
    }
    // generate a hash/color
    if (pcfg == nullptr)
    {
        throw std::logic_error("pcfg is nullptr");
    }
    const CFG &cfg = *pcfg;
    const vector<column_order_t> &v_cci = cfg.v_clrinfl;

    {
        crc32a crc, crc2;
        crc.init();
        crc2.init();
        float w0 = 0.f;
        float w1 = 1.f;
        t.color = 0.75f;
        t.clrc = 1.f;
        t.clry = 1.f;
        for (const column_order_t &cci : v_cci)
        {
            if (cci.used)
            {
                string tmp;
                switch (cci.i)
                {
                    case CCI_REFDES     :
                    {
                        // get only the unique RefDes
                        const std::set<string> urefdes = g.get_refdes();
                        skip_first_delimiter sep(" ");
                        for (const string &val : urefdes) { tmp += sep() + val; }
                    } break;
                    case CCI_FOOTPRINT  : { tmp = g().fp; } break;
                    case CCI_FPLIB      : { tmp = g().fplib; } break;
                    case CCI_SYMBOL     : { tmp = g().part; } break;
                    case CCI_SYMLIB     : { tmp = g().lib; } break;
                    case CCI_DESC       : { tmp = g().desc; } break;
                    case CCI_VALUE      : { tmp = g().value; } break;
                    case CCI_DATASHEET  : { tmp = g().datasheet; } break;
                    case CCI_PKG        : { tmp = g().pkg; } break;
                    case CCI_PARSEDVAL  : { tmp = g().parsedval; } break;
                    //case CCI_DNF        : { tmp = (g().dnf ? "-gn-" : "g"); } break;
                    case CCI_DNF        : { if (g().dnf) { t.clry = 0.701646f; t.clrc = 0.5f; } } break;
                    default:
                    {
                        if (cci.i >= CCI__N)
                        {
                            // custom field
                            if (has_customfield(cci.custom))
                            {
                                const size_t fidx = cci.i - CCI__N;
                                bool has_val = false;
                                const std::set<string> fvals = g.get_field_val(fidx, has_val);
                                for (const string &val : fvals) { tmp += val; }
                                //cout << "clr: " << cci.custom << " " << tmp << "\n";
                            }
                        }
                        else
                        {
                            throw std::logic_error("unknown color_influence field.");
                        }
                    } break;

                };
                if (!tmp.empty())
                {
                    const uint8_t *p = reinterpret_cast<const uint8_t*>(tmp.c_str());
                    crc.process(p, tmp.size());
                    crc2.init();
                    crc2.process(p, tmp.size());
                    double ff = (double)(crc2.crc) / 4294967296.0 - 0.5f;
                    //if (cci.i == CCI_DNF) { ff = g().dnf * 0.5; }
                    t.color += ff * w1;
                    w0 += w1;
                    w1 *= 0.25f;
                }
            }
        }
        //if (w0 != 0.f) { t.color /= w0; }
        t.color -= std::floor(t.color);
        t.csum32 = crc.crc;
    }
    return t;
}

string jiggle_color1(const uint32_t hsh, const float hhh = -1.f, const float targetY = 0.5f, const float chroma = 0.5f)
{
    float h = (hsh/256) / 16777216.f;
    if (hhh >= 0.f) { h = hhh; }

    ghcl_t c;
    c.h = h;
    c.c = chroma;
    c.l = targetY;
    float r,g,b;
    if (c.calc_rgb(r, g, b) == false)
    {
        c.c = 0.f;
        c.calc_rgb(r, g, b);
    }
    constexpr float recg = 1.f/2.4f;
    r = std::pow(r, recg) * 255.f; if (r > 255.f) { r = 255.f; }
    g = std::pow(g, recg) * 255.f; if (g > 255.f) { g = 255.f; }
    b = std::pow(b, recg) * 255.f; if (b > 255.f) { b = 255.f; }
    uint8_t ir = r;
    uint8_t ig = g;
    uint8_t ib = b;
    string s = "rgb("
                + std::to_string(ir) + ','
                + std::to_string(ig) + ','
                + std::to_string(ib) + ')';
    return s;
}
string jiggle_color2(const uint32_t hsh, const float targetY = 0.5f, const float bias = 1.f)
{
    uint8_t ir = hsh>>4;
    uint8_t ig = hsh>>16;
    uint8_t ib = hsh>>24;
    //constexpr float bias = 80.f;
    const float rec = (1.f/(bias+255.f));
    float r = (float)ir+bias; r *= rec;
    float g = (float)ig+bias; g *= rec;
    float b = (float)ib+bias; b *= rec;
    const float y = (0.2126f * r) + (0.7152f * g) + (0.0722 * b);
    //const float y = (r+g+b)/3.f;
    const float gain = targetY/y;
    r *= gain;
    g *= gain;
    b *= gain;
    r = std::pow(r, 0.45) * 255.f; if (r > 255.f) { r = 255.f; }
    g = std::pow(g, 0.45) * 255.f; if (g > 255.f) { g = 255.f; }
    b = std::pow(b, 0.45) * 255.f; if (b > 255.f) { b = 255.f; }
    ir = r;
    ig = g;
    ib = b;
    string s = "rgb("
                + std::to_string(ir) + ','
                + std::to_string(ig) + ','
                + std::to_string(ib) + ')';
    return s;
}

bool CFG::gen_html(std::ostream& os, const vector<column_order_t> &v_co) const
{
    const CFG &cfg = *this;
    os << "<span>" << cfg.name << "</span>\n";

    scoped_tag tag_div(os, "div", "style=\"display: table;\"");
    const string cspan2 = "colspan=\"2\"";
    const string style_disB = "class=\"cfgtd2\"";
    const string style_enaB = "class=\"cfgtd1\"";
    const string style_disC = "class=\"cfgtd4\"";
    const string style_enaC = "class=\"cfgtd3\"";
    const string z_chk = "&#10003; ";
    const string z_xxx = "&#10007; ";
    const string style_chk0 = "class=\"cfgchk0\"";
    const string style_chk1 = "class=\"cfgchk1\"";
    {
        scoped_tag<true> tag_table(os, "table", "class=\"tstats\"" " style=\"float: left\"");
        {
            scoped_tag tag_thead(os, "thead");
            {
                scoped_tag tag_tr(os, "tr");
                {
                    scoped_tag tag_th(os, "th", cspan2);
                    os << "BOM columns";
                }
            }
            {
                scoped_tag tag_tr(os, "tr");
                {
                    { scoped_tag tag_td(os, "td", style_enaB); os << "Enabled"; }
                    { scoped_tag tag_td(os, "td", style_disB); os << "Disabled"; }
                }
            }
        }
        {
            scoped_tag tag_tbody(os, "tbody");
            for (const column_order_t &col : v_co)
            {
                scoped_tag tag_tr(os, "tr");
                {
                    const bool bi = (col.i < CO__X); // built-in or custom field?
                    const string& pre = (col.used ? z_chk : z_xxx);
                    const string& chkstyle = (col.used ? style_chk1 : style_chk0);
                    string style = cspan2
                    + (col.used ? (bi ? style_enaB : style_enaC) : (bi ? style_disB : style_disC));
                    if (bi) { style += " title=\"" + g_column_names[col.i][2] + "\""; }
                    else    { style += " title=\"Custom Field\""; }
                    scoped_tag tag_td(os, "td", style);
                    {
                        scoped_tag tag_sp(os, "span", chkstyle);
                        os << pre;
                    }
                    if (bi)
                    {
                        os << g_column_names[col.i][1];
                    }
                    else
                    {
                        os << col.custom;
                    }
                }
            }
        }
    }
    {
        scoped_tag<true> tag_table(os, "table", "class=\"tstats\"" " style=\"float: left\"");
        {
            scoped_tag tag_thead(os, "thead");
            {
                scoped_tag tag_tr(os, "tr");
                {
                    scoped_tag tag_th(os, "th", cspan2);
                    os << "Grouping by";
                }
            }
            {
                scoped_tag tag_tr(os, "tr");
                {
                    { scoped_tag tag_td(os, "td", style_enaB); os << "Enabled"; }
                    { scoped_tag tag_td(os, "td", style_disB); os << "Disabled"; }
                }
            }
        }
        {
            scoped_tag tag_tbody(os, "tbody");
            for (const column_order_t &cgb : cfg.v_groupby)
            {
                scoped_tag tag_tr(os, "tr");
                {
                    const bool bi = (cgb.i < CGB__X); // built-in or custom field?
                    const string& pre = (cgb.used ? z_chk : z_xxx);
                    const string& chkstyle = (cgb.used ? style_chk1 : style_chk0);
                    string style = cspan2
                    + (cgb.used ? (bi ? style_enaB : style_enaC) : (bi ? style_disB : style_disC));
                    if (bi) { style += " title=\"" + g_cgb_names[cgb.i][1] + "\""; }
                    else    { style += " title=\"Custom Field\""; }
                    scoped_tag tag_td(os, "td", style);
                    {
                        scoped_tag tag_sp(os, "span", chkstyle);
                        os << pre;
                    }
                    if (bi)
                    {
                        os << g_cgb_names[cgb.i][0];
                    }
                    else
                    {
                        os << cgb.custom;
                    }
                }
            }
        }
    }
    {
        scoped_tag<true> tag_table(os, "table", "class=\"tstats\"" " style=\"float: left\"");
        {
            scoped_tag tag_thead(os, "thead");
            {
                scoped_tag tag_tr(os, "tr");
                {
                    scoped_tag tag_th(os, "th", cspan2);
                    os << "Sorting by";
                }
            }
            {
                scoped_tag tag_tr(os, "tr");
                {
                    { scoped_tag tag_td(os, "td", style_enaB); os << "Enabled"; }
                    { scoped_tag tag_td(os, "td", style_disB); os << "Disabled"; }
                }
            }
        }
        {
            scoped_tag tag_tbody(os, "tbody");
            for (const column_order_t &cso : cfg.v_sortorder)
            {
                scoped_tag tag_tr(os, "tr");
                {
                    const bool bi = (cso.i < CSO__X); // built-in or custom field?
                    const string& pre = (cso.used ? z_chk : z_xxx);
                    const string& chkstyle = (cso.used ? style_chk1 : style_chk0);
                    string style = cspan2
                    + (cso.used ? (bi ? style_enaB : style_enaC) : (bi ? style_disB : style_disC));
                    if (bi) { style += " title=\"" + g_cso_names[cso.i][1] + "\""; }
                    else    { style += " title=\"Custom Field\""; }
                    scoped_tag tag_td(os, "td", style);
                    {
                        scoped_tag tag_sp(os, "span", chkstyle);
                        os << pre;
                    }
                    if (bi)
                    {
                        os << g_cso_names[cso.i][0];
                    }
                    else
                    {
                        os << cso.custom;
                    }
                }
            }
        }
    }
    os << '\n';
    return true;
}

bool STATS::gen_html(std::ostream& os)
{
    //scoped_tag<true> tag_table(os, "table", "class=\"tstats\"");
    {
        //scoped_tag tag_thead(os, "thead");
        {
            scoped_tag tag_tr(os, "tr");
            {
                scoped_tag tag_th(os, "th", "colspan=\"2\"");
                if (hack)   { os << "Summary"; }
                else        { os << xml_name; }
            }
        }
    }
    {
        const string style_misc = "class=\"statstd2\"";
        scoped_tag tag_tbody(os, "tbody");
        if (hack == false)
        {
            {
                scoped_tag tag_tr(os, "tr");
                { scoped_tag tag_td(os, "td"); os << source; }
                { scoped_tag tag_td(os, "td"); os << title; }
            }
            {
                scoped_tag tag_tr(os, "tr");
                { scoped_tag tag_td(os, "td"); os << "Revision"; }
                { scoped_tag tag_td(os, "td"); os << rev; }
            }
            {
                scoped_tag tag_tr(os, "tr");
                { scoped_tag tag_td(os, "td"); os << "Date"; }
                { scoped_tag tag_td(os, "td"); os << date2; }
            }
            {
                scoped_tag tag_tr(os, "tr");
                { scoped_tag tag_td(os, "td"); os << "Company"; }
                { scoped_tag tag_td(os, "td"); os << company; }
            }
            {
                scoped_tag tag_tr(os, "tr");
                { scoped_tag tag_td(os, "td", style_misc); os << "Comments"; }
                {
                    scoped_tag tag_td(os, "td", style_misc);
                    {
                        if (!comments.empty())
                        {
                            scoped_tag tag_meh(os, "ol", "style=\"margin: 0;\"");
                            for (string &s : comments)
                            {
                                if (!s.empty()) { os << "<li>" << s << "</li>\n"; }
                            }
                        }
                    }
                }
            }
            {
                scoped_tag tag_tr(os, "tr");
                { scoped_tag tag_td(os, "td", style_misc); os << "Generated with"; }
                { scoped_tag tag_td(os, "td", style_misc); os << tool; }
            }
            {
                scoped_tag tag_tr(os, "tr");
                { scoped_tag tag_td(os, "td", style_misc); os << "Date"; }
                { scoped_tag tag_td(os, "td", style_misc); os << date; }
            }
        }
        {
            scoped_tag tag_tr(os, "tr");
            { scoped_tag tag_td(os, "td"); os << "Board count"; }
            { scoped_tag tag_td(os, "td"); os << n_board_count; }
        }
        {
            scoped_tag tag_tr(os, "tr");
            { scoped_tag tag_td(os, "td"); os << "Unique components"; }
            { scoped_tag tag_td(os, "td"); os << n_unique_components; }
        }
        {
            scoped_tag tag_tr(os, "tr");
            { scoped_tag tag_td(os, "td"); os << "Components"; }
            { scoped_tag tag_td(os, "td"); os << n_components; }
        }
    }
    os << '\n';
    return true;
}

vector<column_order_t> gen_co(BOM &bom)
{
    vector<column_order_t> vec;
    if (bom.pcfg == nullptr)
    {
        throw std::logic_error("pcfg is nullptr");
    }
    const CFG &cfg = *bom.pcfg;
    const vector<column_order_t> &v_co = cfg.v_colorder;

    {
        // debug
        //for (const column_order_t &co : v_co) { cout << "co: " << int(co.used) << " " << co.i << ": " << co.custom << "\n"; }
        //for (auto &x : v_field_names) { cout << ">> " << x << "\n"; }
    }

    // scan the v_co for all the custom fields
    // find which of them are used and wanted
    // then remember the remaining ones
    // the remaining ones should be inserted at the ALLCUSTOM position

    // +1 == wanted, -1 == unwanted, 0 == remaining (these go to ALLCUSTOM)
    vector<int> cflut(bom.v_field_names.size(), 0);
    //
    for (const column_order_t &co : v_co)
    {
        if (co.i >= CO__N)
        {
            // custom field, is it available in the BOM?
            if (co.used)    { cout << "Wanted..: "; }
            else            { cout << "Unwanted: "; }
            cout << co.custom << " - ";
            if (bom.has_customfield(co.custom))
            {
                size_t i = bom.lu_field(co.custom);
                assert(i < bom.v_field_names.size());
                cout << "Found it: " << i << "\n";
                cflut[i] = (co.used ? +1 : -1);
            }
            else
            {
                cout << "\n";
            }
        }
    }
    // the cflut elements that were not touched (their value would be 0)
    // correspond to custom fields that are "remaining"

    for (const column_order_t &co : v_co)
    {
        if (co.used)
        {
            switch (co.i)
            {
                case CO_ALLCUSTOM: // this case shall not show up in the html gen code
                {
                    // put all the remaining fields here
                    size_t i = 0;
                    while (i < cflut.size())
                    {
                        if (cflut[i] == 0)
                        {
                            column_order_t tmp;
                            tmp.used = true;
                            tmp.custom = bom.v_field_names[i];
                            tmp.i = (CO__N + i);
                            // html generating code can just de-bias this instead of
                            // looking up by the .custom string
                            vec.push_back(tmp);
                        }
                        ++i;
                    }
                } break;
                case CO_REF:
                case CO_PV:
                case CO_PKG:
                case CO_VAL:
                case CO_FP:
                case CO_COUNT:
                case CO_TOTAL:
                case CO_DESC:
                case CO_DATASH:
                case CO_SYMB:
                case CO_SRCBOM:
                case CO_PARSEDVAL:
                case CO_CHECKB:
                {
                    // just copy
                    vec.push_back(co);
                } break;

                default:
                {
                    if (co.i >= CO__N)
                    {
                        // an explicitly specified custom field
                        // okay, explicitly specified custom fields should appear in the html
                        // even if the BOM doesn't have them.. for consistency between projects
                        // so synthesize some special column_order_t object here
                        if (bom.has_customfield(co.custom)) // lu_field() crashes otherwise
                        {
                            column_order_t tmp;
                            tmp = co;
                            // recalculate the index (is this needed? better do it)
                            size_t i = bom.lu_field(co.custom);
                            assert(i < bom.v_field_names.size());
                            tmp.i = (CO__N + i);
                            vec.push_back(tmp);
                        }
                        else
                        {
                            column_order_t tmp;
                            tmp = co;
                            tmp.i = (-1);
                            vec.push_back(tmp);
                        }
                    }
                    else { throw std::logic_error("gen_co(): unexpected column order."); }
                } break;
            };
        }
    }
    return vec;
}

string z_td_class_fmt(const string &what, const bool dnf = false)
{
    string s = "class=\"";
    if (dnf) { s += "dnf "; }
    s += what;
    s += '\"';
    return s;
}

void z_put_quotes(std::ostream& os, const string& s, const bool always = true)
{
    bool doit = always;
    if (always == false)
    {
        // TODO: figure out whether it's needed
    }
    if (doit)   { os << '\"' << s << '\"'; }
    else        { os << s; }
}

template<const char _first = '\"', const char _last = '\"'>
struct scoped_quotes
{
    scoped_quotes(std::ostream& _os)
    : os(_os)
    {
        os << _first;
    }
    ~scoped_quotes()
    {
        os << _last;
    }
    private:
    std::ostream &os;
};

bool BOM::gen_csv(std::ostream& os)
{
    if (pcfg == nullptr)
    {
        cerr << "Internal error: pcfg is nullptr\n";
        return false;
    }
    [[maybe_unused]] const CFG &cfg = *pcfg;
    vector<column_order_t> v_co = gen_co(*this);

    const string cell_nl = "\n";
    size_t k = 0;

    {
        k = 0;
        while (k < v_co.size())
        {
            const column_order_t &co = v_co[k];
            if (co.used)
            {
                string style = "";
                switch (co.i)
                {
                    case CO_REF:
                    case CO_PV:
                    case CO_PKG:
                    case CO_VAL:
                    case CO_FP:
                    case CO_COUNT:
                    case CO_TOTAL:
                    case CO_DESC:
                    case CO_DATASH:
                    case CO_SYMB:
                    case CO_SRCBOM:
                    case CO_PARSEDVAL:
                    case CO_CHECKB:
                    {
                        z_put_quotes(os, g_column_names[co.i][1]);
                    } break;
                    case CO_ALLCUSTOM: // this shouldn't happen
                    {
                        os << "Custom fields";
                    } break;

                    default:
                    {
                        if (co.i >= CO__N)
                        {
                            z_put_quotes(os, co.custom);
                        }
                        else
                        {
                            cerr << "Internal error: broken custom field?\n";
                        }
                    } break;
                };
                os << cfg.csv_delim;
            }
            ++k;
        }
        //os << '\n';
    }
    {
        size_t rown = 0; // used for generating IDs
        for (GROUP &g : v_group)
        {
            ++rown;
            os << '\n';
            tbl_group_t tg = prep(g);
            bool dnf = g().dnf; // Note: the group may have a mix of DNF values..
            size_t coln = 0; // used for generating IDs
            for (const column_order_t &co : v_co)
            {
                ++coln;
                if (co.used == false) { continue; }
                switch (co.i)
                {
                    case CO_REF:
                    {
                        scoped_quotes sq(os);
                        skip_first_delimiter sb(" ");
                        for (string &s : tg.ref)
                        {
                            os << sb();
                            os << s;
                        }
                    } break;
                    case CO_PV:
                    {
                        string x = g.get_pv_html(); // TODO
                        z_put_quotes(os, x);
                    } break;
                    case CO_PKG:
                    {
                        skip_first_delimiter br(cell_nl);
                        scoped_quotes sq(os);
                        for (pkg_fp_lib_t &x : tg.pkg)
                        {
                            os << br();
                            os << x.pkg;
                        }
                    } break;
                    case CO_VAL:
                    {
                        skip_first_delimiter br(cell_nl);
                        scoped_quotes sq(os);
                        for (parsedval_t &x : tg.pval)
                        {
                            os << br();
                            os << x.value;
                        }
                    } break;
                    case CO_FP:
                    {
                        skip_first_delimiter br(cell_nl);
                        scoped_quotes sq(os);
                        for (namenlib_t &x : tg.fp)
                        {
                            os << br();
                            os << x.name;
                        }
                    } break;
                    case CO_COUNT:
                    { scoped_quotes sq(os); os << g.get_count(); } break;
                    case CO_TOTAL:
                    { scoped_quotes sq(os); os << g.get_total(); } break;
                    case CO_DESC:
                    {
                        skip_first_delimiter hr(cell_nl);
                        scoped_quotes sq(os);
                        for (string &x : tg.desc)
                        {
                            os << hr();
                            os << x;
                        }
                    } break;
                    case CO_DATASH:
                    {
                        skip_first_delimiter br(cell_nl);
                        scoped_quotes sq(os);
                        for (string &x : tg.datash)
                        {
                            if (!x.empty()) { os << br(); }
                            os << x;
                        }
                    } break;
                    case CO_SYMB:
                    {
                        skip_first_delimiter br(cell_nl);
                        scoped_quotes sq(os);
                        for (namenlib_t &x : tg.sym)
                        {
                            os << br();
                            os << x.name;
                        }
                    } break;
                    case CO_SRCBOM:
                    { scoped_quotes sq(os); os << g.get_srcbom(cell_nl); } break;
                    case CO_PARSEDVAL:
                    { scoped_quotes sq(os); os << g().parsedval; } break;
                    case CO_CHECKB:
                    {
                        os << "\"[_]\"";
                    } break;

                    default:
                    {
                        if (co.i >= CO__N)
                        {
                            if (co.i == size_t(-1))
                            {
                                // empty cell!
                                os << "\"\"";
                            }
                            else
                            {
                                const size_t fidx = (co.i - CO__N);
                                assert(fidx < v_field_names.size());
                                {
                                    skip_first_delimiter br(cell_nl);
                                    scoped_quotes sq(os);

                                    bool has_val = false;
                                    for (const string &x : g.get_field_val(fidx, has_val))
                                    {
                                        os << br();
                                        os << x;
                                    }
                                }
                            }
                        }
                        else { throw std::logic_error("unexpected column order."); }
                    } break;
                };
                os << cfg.csv_delim;
            }
        }
    }
    return true;
}

bool BOM::gen_html(std::ostream& os)
{
    if (pcfg == nullptr)
    {
        cerr << "Internal error: pcfg is nullptr\n";
        return false;
    }
    [[maybe_unused]] const CFG &cfg = *pcfg;
    //const vector<column_order_t> &v_co = cfg.v_colorder;
    vector<column_order_t> v_co = gen_co(*this);

    {
        // debug
        //for (const column_order_t &co : v_co) { cout << "co: " << int(co.used) << " " << co.i << ": " << co.custom << "\n"; }
        //for (auto &x : v_field_names) { cout << ">> " << x << "\n"; }
    }

    size_t k = 0;
    os << "<!DOCTYPE html>\n";
    scoped_tag<true> tag_html(os, "html");
    {
        scoped_tag<true> ttt(os, "head");
        os << "<meta charset=\"UTF-8\">";
        {
            scoped_tag tag_title(os, "title");
            if (v_stats.size() > 1)
            {
                os << "Merged BOM";
            }
            else
            {
                // single BOM
                os << "BOM: " << v_stats[0].title2;
            }
        }
    }
    scoped_tag<true> tag_body(os, "body", "style=\"background-color: #111;color: lightgray; font-family:Geneva,Verdana,sans-serif;\"");
    {
        scoped_tag tag_h2(os, "h2");
        os << "Auto-generated BOM";
    }
    {
        scoped_tag tag_meh(os, "span");
        os << "Config: " << cfg.name;
    }
    os << '\n';
    {
        // <style> woz here
        {
            scoped_tag<true> tag_table(os, "table", "class=\"tstats\"");
            os << "<caption><div class=\"stickycaption\">Statistics:</div></caption>\n";
            for (STATS &stats : v_stats) { stats.gen_html(os); }
        }
        os << "<br/>\n";
        //scoped_tag<true> tag_table(os, "table", "border=\"1\" style=\"border-collapse:collapse\" align=\"center\"");
        scoped_tag<true> tag_table(os, "table", "class=\"tg\"");
        os << "<caption><div class=\"stickycaption\">Bill of materials:</div></caption>\n";
        {
            scoped_tag<true> tag_thead(os, "thead");
            {
                scoped_tag tag_tr(os, "tr");
                os << '\n';
                #if 0
                k = 0;
                while (k < v_colorder.size())
                {
                    size_t co = v_colorder.at(k);
                    switch (co)
                    {
                        case CO_REF:
                        case CO_PV:
                        case CO_COUNT:
                        case CO_TOTAL:
                        case CO_DESC:
                        case CO_DATASH:
                        case CO_SYMB:
                        case CO_SRCBOM:
                        case CO_CHECKB:
                        {
                            scoped_tag tag_th(os, "th");
                            os << g_column_names[co];
                        } break;
                        case CO_ALLCUSTOM:
                        {
                            scoped_tag tag_th(os, "th");
                            os << "Custom fields";
                        } break;

                        default:
                        {
                            if (co >= CO__N)
                            {
                                // TODO: custom fields
                            }
                        } break;
                    };
                    ++k;
                }
                #else
                // spew out the column names
                #ifndef NO_ROW_NUMBER
                { scoped_tag tag_th(os, "th", "class=\"rnc\" style=\"z-index: 3;\""); os << "N*"; }
                #endif // NO_ROW_NUMBER
                k = 0;
                while (k < v_co.size())
                {
                    const column_order_t &co = v_co[k];
                    if (co.used)
                    {
                        string style = "";
                        switch (co.i)
                        {
                            case CO_REF:
                            {
                                style = "title=\"" + g_column_names[co.i][2] + '\"';
                                style += " style=\"min-width: 7em;\"";
                                scoped_tag tag_th(os, "th", style);
                                os << g_column_names[co.i][0];
                            } break;
                            case CO_PV:
                            case CO_PKG:
                            case CO_VAL:
                            case CO_FP:
                            case CO_COUNT:
                            case CO_TOTAL:
                            case CO_DESC:
                            case CO_DATASH:
                            case CO_SYMB:
                            case CO_SRCBOM:
                            case CO_PARSEDVAL:
                            case CO_CHECKB:
                            {
                                style = "title=\"" + g_column_names[co.i][2] + '\"';
                                scoped_tag tag_th(os, "th", style);
                                os << g_column_names[co.i][0];
                            } break;
                            case CO_ALLCUSTOM: // this shouldn't happen
                            {
                                scoped_tag tag_th(os, "th");
                                os << "Custom fields";
                            } break;

                            default:
                            {
                                if (co.i >= CO__N)
                                {
                                    // TODO: custom fields
                                    //const size_t fidx = (co.i - CO__N);
                                    scoped_tag tag_th(os, "th", "class=\"thcf\" title=\"Custom field\"");
                                    os << co.custom;
                                }
                                else
                                {
                                    cerr << "Internal error: broken custom field?\n";
                                }
                            } break;
                        };
                        os << '\n';
                    }
                    ++k;
                }
                #endif // 1
            }
        }
        os << '\n';
        vector<string> pv_clr(v_group.size());
        {
            scoped_tag<true> tag_tbody(os, "tbody");
            size_t rown = 0; // used for generating IDs
            for (GROUP &g : v_group)
            {
                ++rown;
                os << '\n';
                tbl_group_t tg = prep(g);
                bool dnf = g().dnf; // Note: the group may have a mix of DNF values..
                //cout << "-\n";
                //float hhh = (rown-1.f) / (float)(v_group.size());
                //hhh *= 5.f;
                //float hhh = -1.f;
                float hhh = tg.color;
                pv_clr.at(rown-1) = jiggle_color1(tg.csum32, hhh, tg.clry * 0.021012f, tg.clrc * 0.5f);
                //string styler = "class=\"trR" + std::to_string(rown) + '\"';
                scoped_tag tag_tr(os, "tr", z_td_class_fmt("trR" + std::to_string(rown), dnf));
                os << '\n';
                #ifndef NO_ROW_NUMBER
                { scoped_tag tag_td(os, "td", "class=\"rn\""); os << rown; }
                #endif // NO_ROW_NUMBER
                size_t coln = 0; // used for generating IDs
                for (const column_order_t &co : v_co)
                {
                    ++coln;
                    string cell_id = "cR" + std::to_string(rown) + 'C' + std::to_string(coln);
                    if (co.used == false) { continue; }
                    switch (co.i)
                    {
                        case CO_REF:
                        {
                            scoped_tag tag_td(os, "td", z_td_class_fmt("tdref",dnf));
                            skip_first_delimiter sb(" ");
                            for (string &s : tg.ref)
                            {
                                os << sb();
                                os << s;
                            }
                            //os << g.get_ref(" ");
                        } break;
                        case CO_PV:
                        {
                            //scoped_tag tag_td(os, "td", "align=\"left\"");
                            //os << g.get_pv_html();
                            string x = g.get_pv_html();
                            //std::hash<string> h;
                            //size_t t = h(x);
                            //string style = "class=\"tdpv\"";
                            string style = "class=\"tdpv" + std::to_string(rown) + '\"';
                            // color=rgb(255, 99, 71)
                            #if 0
                            style += " style=\"background-color: rgb("
                            + std::to_string(t      &0xFF) + ','
                            + std::to_string((t>>16)&0xFF) + ','
                            + std::to_string((t>>24)&0xFF) + ")\"";
                            #endif // 0
                            //pv_clr.at(rown-1) = jiggle_color1(t, 0.03f, 60.f);
                            /*
                            style += " style=\"background-color: "
                                    + jiggle_color1(t, 0.03f, 60.f)
                                    + "\"";
                            */
                            scoped_tag tag_td(os, "td", style);
                            os << x;
                        } break;
                        case CO_PKG:
                        {
                            scoped_tag tag_td(os, "td", z_td_class_fmt("zpkg", dnf));
                            skip_first_delimiter br("<br/>");
                            for (pkg_fp_lib_t &x : tg.pkg)
                            {
                                string ttl = "title=\"";
                                ttl += "Footprint: " + x.fp + g_html_newline
                                    + "Library: " + x.lib
                                    + '\"';
                                os << br();
                                scoped_tag tag_span(os, "span", ttl);
                                os << x.pkg;
                            }
                        } break;
                        case CO_VAL:
                        {
                            scoped_tag tag_td(os, "td", z_td_class_fmt("zval", dnf));
                            skip_first_delimiter br("<br/>");
                            for (parsedval_t &x : tg.pval)
                            {
                                os << br();
                                string ttl = "title=\"" + x.optimized + x.remainder + '\"';
                                scoped_tag tag_span(os, "span", ttl);
                                os << x.value;
                            }
                        } break;
                        case CO_FP:
                        {
                            scoped_tag tag_td(os, "td", z_td_class_fmt("zfp", dnf));
                            skip_first_delimiter br("<br/>");
                            for (namenlib_t &x : tg.fp)
                            {
                                string ttl = "title=\"";
                                ttl += "Library: " + x.lib
                                    + '\"';
                                os << br();
                                scoped_tag tag_span(os, "span", ttl);
                                os << x.name;
                            }
                        } break;
                        case CO_COUNT:
                        { scoped_tag tag_td(os, "td", z_td_class_fmt("tdqty", dnf)); os << g.get_count(); } break;
                        case CO_TOTAL:
                        { scoped_tag tag_td(os, "td", z_td_class_fmt("tdqty2", dnf)); os << g.get_total(); } break;
                        case CO_DESC:
                        {
                            scoped_tag tag_td(os, "td", z_td_class_fmt("zdesc", dnf));
                            skip_first_delimiter hr("<hr/>");
                            for (string &x : tg.desc)
                            {
                                os << hr();
                                os << x;
                            }
                        } break;
                        case CO_DATASH:
                        {
                            scoped_tag tag_td(os, "td", z_td_class_fmt("zdatash", dnf));
                            skip_first_delimiter br(" ");
                            for (string &x : tg.datash)
                            {
                                if (!x.empty()) { os << br(); }
                                if (x.length() < 4) { os << x; }
                                else
                                {
                                    string ttl = "href=\"";
                                    ttl += x + '\"';
                                    ttl += " title=\"" + x + '\"';

                                    scoped_tag tag_span(os, "a", ttl);
                                    os << g_html_link_icon << "link";
                                }
                            }
                            /*
                            bool first = true;
                            for (const string &x : g.get_datasheet())
                            {
                                if (first == false) { os << ' '; }
                                first = false;
                                if (x.length() < 4) { os << x; }
                                else { os << "<a href=\"" << x << "\">link</a>"; }
                            }
                            */
                        } break;
                        case CO_SYMB:
                        {
                            scoped_tag tag_td(os, "td", z_td_class_fmt("zsym", dnf));
                            //os << g.get_symb_html();
                            skip_first_delimiter br("<br/>");
                            for (namenlib_t &x : tg.sym)
                            {
                                string ttl = "title=\"";
                                ttl += "Library: " + x.lib
                                    + '\"';
                                os << br();
                                scoped_tag tag_span(os, "span", ttl);
                                os << x.name;
                            }
                        } break;
                        case CO_SRCBOM:
                        { scoped_tag tag_td(os, "td", z_td_class_fmt("zsrcbom")); os << g.get_srcbom("<br/>"); } break;
                        case CO_PARSEDVAL:
                        {
                            scoped_tag tag_td(os, "td");
                            skip_first_delimiter br("<hr/>");
                            for (parsedval_t &x : tg.pval)
                            {
                                if (!x.optimized.empty())
                                {
                                    string ttl = x.optimized;
                                    if (!ttl.empty()) { ttl = "title=\"" + ttl + '\"'; }
                                    os << br();
                                    scoped_tag tag_span(os, "span", ttl);
                                    os << x.optimized + x.remainder;
                                }
                            }
                        } break;
                        case CO_CHECKB:
                        {
                            //scoped_tag tag_th(os, "td");
                            //os << "<input type=\"checkbox\" size=\"200%\"/>";
                            #if 1
                            scoped_tag tag_td(os, "td", z_td_class_fmt("cbc container", dnf));
                            os <<
                            "<input type=\"checkbox\"/> <span class=\"checkmark\"></span>";
                            #else
                            string aaa = "id=\"" + cell_id + "\"";
                            aaa += " class=\"cbc\"";
                            scoped_tag tag_td(os, "td", aaa);
                            os <<
                            "<label class=\"container\">"
                            "<input type=\"checkbox\"/> <span class=\"checkmark\"></span>"
                            "</label>";
                            #endif // 1
                        } break;

                        default:
                        {
                            if (co.i >= CO__N)
                            {
                                #if 1
                                if (co.i == size_t(-1))
                                {
                                    // empty cell!
                                    scoped_tag tag_td(os, "td");
                                }
                                else
                                {
                                    const size_t fidx = (co.i - CO__N);
                                    assert(fidx < v_field_names.size());
                                    {
                                        std::ostringstream oss;
                                        //scoped_tag tag_td(os, "td"); // background-color: #8C8C8C
                                        bool first = true;

                                        //const size_t fidx = lu_field(co.custom);
                                        bool has_val = false;
                                        for (const string &x : g.get_field_val(fidx, has_val))
                                        {
                                            if (first == false) { oss << "<hr>"; }
                                            first = false;
                                            oss << x;
                                        }
                                        string style = z_td_class_fmt("tdcf", dnf);
                                        if (has_val == false) { style.clear(); }
                                        scoped_tag tag_td(os, "td", style);
                                        os << oss.str();
                                    }
                                }
                                #else
                                //if ((co.used) && (has_customfield(co.custom)))
                                if (has_customfield(co.custom))
                                {
                                    std::ostringstream oss;
                                    //scoped_tag tag_td(os, "td"); // background-color: #8C8C8C
                                    bool first = true;

                                    const size_t fidx = lu_field(co.custom);
                                    bool has_val = false;
                                    for (const string &x : g.get_field_val(fidx, has_val))
                                    {
                                        if (first == false) { oss << "<br/>"; }
                                        first = false;
                                        oss << x;
                                    }
                                    string style = "class=\"tdcf\"";
                                    if (has_val == false) { style.clear(); }
                                    scoped_tag tag_td(os, "td", style);
                                    os << oss.str();
                                }
                                else
                                {
                                    //cout << "skipped(" << int(co.used) << ") " << co.custom << "\n";
                                    scoped_tag tag_td(os, "td");
                                    os << "skipped: " << co.custom;
                                }
                                #endif // 1
                            }
                            #if 1
                            else { throw std::logic_error("unexpected column order."); }
                            #else
                            #warning "uncomment the above ELSE!"
                            else { scoped_tag tag_td(os, "td"); os << g.size(); }
                            #endif // 0
                        } break;
                        //{ scoped_tag tag_td(os, "td"); os << g.size(); } break;
                    };
                    os << '\n';
                }
            }
        }

        {
            scoped_tag<true> tag_style(os, "style", "type=\"text/css\"");
            os <<
                    //".body{background-color: #333;}\n"
                    "a:link { color: #00abff; text-decoration: none; }\n"
                    "a:visited { color: #8855ff; }\n"
                    "a:hover { color: white; }\n"
                    // ---------
                    ".tstats{\n"
                        "border-collapse:collapse;border-spacing:0;"
                        "border-color:black;border-style:solid;border-width:1px;"
                        //"font-family:monospace;font-size:small;font-weight:normal;"
                        "overflow:hidden;padding:10px 5px;word-break:normal;"
                    "}\n"
                    ".tstats th{\n" // STATS table header
                        "background-color:#3c3c3c;"
                        "color: #ffc14a;"
                        "border-color:black;border-style:solid;border-width:1px;"
                        "font-family:'Courier New',Courier,monospace;font-size:small;"
                        //"font-weight:normal;"
                        "overflow:hidden;padding:5px 5px;word-break:normal;"
                    "}\n"
                    ".tstats td{\n" // STATS TD
                        "border-color:#222;border-style:solid;border-width:1px;"
                        "background-color: #333;"
                        "color:#ffffff;"
                        "font-family:'Courier New',Courier,monospace;font-size:small;overflow:hidden;"
                        "padding:0px 5px;"
                        "word-break:keep-all;"
                    "}\n"
                    ".statstd2 {\n" // STATS TD, less important data
                        "color: #ac8963 !important;"
                    "}\n"
                    // ------------
                    ".cfgtd1 { color: #ffffff !important; background-color: #20233c !important;}\n"
                    ".cfgtd2 { color: #9f9f9f !important; background-color: #1a1c28 !important;}\n"
                    ".cfgtd3 { color: #22ff64 !important; background-color: #2c3b3e !important;}\n"
                    ".cfgtd4 { color: #71ac56 !important; background-color: #1d2728 !important;}\n"
                    ".cfgchk0 { color: #c8423c !important;}\n"
                    ".cfgchk1 { color: #00ff87 !important;}\n"
                    // ------------
                    // ------------
                    ".tg {border-collapse: separate;border-spacing:0px;}\n"

                    ".tg th{\n" // table header
                        //"border-collapse:collapse;"
                        "background-color:#20233c;"
                        "color: #ffc14a;"
                        "border-color:black;border-style:solid;border-width:1px;"
                        "font-family:'Courier New',Courier,monospace;font-size:small;font-weight:normal;"
                        "overflow:hidden;padding:10px 5px;word-break:normal;"
                        "z-index: 2;"
                        "position:-webkit-sticky;position:sticky;text-align:left;top:-1px;vertical-align:top;will-change:transform"
                    "}\n"

                    ".thcf{\n" // for the custom field header
                        "color: #00e87c !important;"
                        "background-color: #2c3b3e !important;"
                        "font-style: italic;"
                    "}\n"

                    ".rn, .rnc{\n"
                        "position: sticky; left: 0;"
                        "z-index: 2;"
                        "background-color: #20233c;"
                        "color: #dd9b14 !important;"
                        "border-color: black !important;"
                    "}\n"
                    ".rnth{ z-order: 3;}\n"

                    ".tdcf{\n" // for the custom field td
                        "background-color: #304230;"
                        "color: yellowgreen !important;"
                    "}\n"
                    ".tdcf hr{\n" // horizontal line for custom fields td
                        "background-color: #866d20 !important;"
                    "}\n"
                    ".tdcf.dnf { background-color: #30423055; }\n"

                    ".tdpv{text-align: left;}\n"
                    ".tdqty{text-align: right;}\n"
                    ".tdqty2{text-align: right;}\n"

                    ".tg tr{ /*background-color: #333;*/ }\n"
                    ".tg td{\n" // TD
                        "border-color:#2c2c2c;border-style:solid;border-width:1px;"
                        //"background-color: #333;"
                        //"color:#ffffff;"
                        "font-family:'Courier New',Courier,monospace;font-size:small;overflow:hidden;"
                        "padding:0.3125rem;word-break:keep-all;"
                    "}\n"

                    ".tg hr{"
                        "height: 1px;border-width: 0;background-color: #6a4343;margin-top: 2px;margin-bottom: 2px;"
                    "}\n"

                    ".stickycaption{\n"
                        "display: table;text-align: center;position: sticky;left: 50%;"
                    "}\n"

                    ".tg-sort-header::-moz-selection{background:0 0}\n"
                    ".tg-sort-header::selection{background:0 0}.tg-sort-header{cursor:pointer}\n"
                    ".tg-sort-header:after{content:'';float:right;border-width:0 5px 5px;border-style:solid;border-color:#3d8fd7 transparent;visibility:hidden}\n"
                    ".tg-sort-header:hover:after{visibility:visible}\n"
                    ".tg-sort-asc:after,.tg-sort-asc:hover:after,.tg-sort-desc:after{visibility:visible;opacity:.4}\n"
                    ".tg-sort-desc:after{border-bottom:none;border-width:5px 5px 0}\n"

                    ".container {\n"
                        //"display: block;"
                        "position: relative;"
                        //"padding-left: 35px;margin-bottom: 12px;"
                        "padding-left: 1;margin-bottom: 1;"
                        //"cursor: pointer;"
                        "-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"
                    "}\n"

                    ".container input {\n"
                        "position: relative;"
                        "z-index: 1;"
                        //"opacity: 0;"
                        "cursor: pointer;"
                        //"height: 0;width: 0;"
                    "}\n"
                    ".checkmark {\n"
                        //"position: absolute;top: 0;left: 0;height: 25px;width: 25px;"
                        "position: absolute;top: 0;left: 0;""height: 100%;width: 100%;"
                        //"z-index: -2;"
                        "background-color: #ac3434;" // not pressed
                    "}\n"
                    ".container.dnf input:checked ~ .checkmark {background-color: #024f3b6b;}\n"
                    ".container input:checked ~ .checkmark {background-color: #024f3b;}\n"
                    ".container.dnf .checkmark { background-color: #ac34342e; }\n"

                    ".tdref{\n"
                        "color: #939393 !important;\n"
                        "max-width: 20em;\n"
                    "}\n"
                    ".cbc { padding: 0 !important; margin: 0 !important; text-align: center; }\n"
                    ".bvp_clr{ color: #f5b3ff; }\n"
                    ".bvs_clr{ color: #ff8f00; }\n"
                    ".bvv_clr{ color: #b1ffc1; }\n"

                    // row hover
                    ".tg * tr:hover > td, tr:hover .bvv_clr {\n"
                        "background-color: #004a37 !important;\n"
                        "color: #fff !important;\n"
                        //"font-weight: bold !important;"
                        "border-color: #246050;\n"
                        "border-inline-color: #004634;\n"
                    "}\n"
                    // DNF row hover
                    ".tg * tr.dnf:hover > td {\n"
                        "color: #b3907f !important;\n"
                        "background-color: #553930 !important;\n"
                        "border-color: #644f46 !important;\n"
                        "border-inline-color: #4d342c !important;\n"
                    "}\n"

                    ".tg * tr.dnf:hover > .rn {\n"
                        "color: #b35c42 !important;\n"
                        "background-color: #403636 !important;\n"
                        "border-inline-color: #422c25 !important;\n"
                    "}\n"

                    ".tg * tr:hover > .rn {\n"
                        "background-color: #013942 !important;\n"
                        "color: #88ae00 !important;\n"
                        "border-color: #246050 !important;\n"
                        "background-color: #1a3653 !important;\n"
                        "border-inline-color: #00392b !important;\n"
                    "}\n"

                    ".dnf { color: #000000 !important; }\n"
                    ".zpkg{text-align: right; color: #bb8cff;}\n"
                    ".zval{text-align: left; color: aquamarine;}\n"
                    ".zfp{color: #80a0ac;}\n"
                    ".zsym{color: #e87b7b;}\n"
                    ".zdesc{\n" // description TD
                        "color: #aaaaaa;"
                        "font-family: Geneva,Verdana,sans-serif !important; font-size: 0.75em !important;"
                    "}\n"
                    ".zdatash {font-size: 0.75em !important;}\n"
                    ".zsrcbom {font-size: 0.75em !important;}\n"

                    // .tg * tr:hover > td, :hover>span { background-color: #001111 !important; color:  greenyellow !important; }
                ;
            {
                // this is not great right now, since the PV column might not have been used
                // TODO: perhaps a hash/color should be pre-calculated before gen_html()
                // ..from the values of the columns used for "grouping"
                size_t k = 0;
                for (const string &s : pv_clr)
                {
                    os << ".trR" << k+1 << "{background-color: " << s << ";}\n";
                    ++k;
                }
            }
        }
    }
    {
        cfg.gen_html(os, v_co);
    }
            {
                scoped_tag tag_small(os, "small");
                os << "This document was automatically-butchered-together by ";
                {
                    scoped_tag tag_strong(os, "strong");
                    os << appname << " v";
                    print_version(os, appversion);
                }
                os << " <a href=" << appwebsite << ">(" << g_html_link_icon << "visit)</a>\n";
            }
# if 0
    os <<
"<script charset=\"utf-8\">var TGSort=window.TGSort||function(n){\"use strict\";function r(n){return n?n.length:0}function t(n,t,e,o=0){for(e=r(n);o<e;++o)t(n[o],o)}function e(n){return n.split(\"\").reverse().join(\"\")}function o(n){var e=n[0];return t(n,function(n){for(;!n.startsWith(e);)e=e.substring(0,r(e)-1)}),r(e)}function u(n,r,e=[]){return t(n,function(n){r(n)&&e.push(n)}),e}var a=parseFloat;function i(n,r){return function(t){var e=\"\";return t.replace(n,function(n,t,o){return e=t.replace(r,\"\")+\".\"+(o||\"\").substring(1)}),a(e)}}var s=i(/^(?:\\s*)([+-]?(?:\\d+)(?:,\\d{3})*)(\\.\\d*)?$/g,/,/g),c=i(/^(?:\\s*)([+-]?(?:\\d+)(?:\\.\\d{3})*)(,\\d*)?$/g,/\\./g);function f(n){var t=a(n);return!isNaN(t)&&r(\"\"+t)+1>=r(n)?t:NaN}function d(n){var e=[],o=n;return t([f,s,c],function(u){var a=[],i=[];t(n,function(n,r){r=u(n),a.push(r),r||i.push(n)}),r(i)<r(o)&&(o=i,e=a)}),r(u(o,function(n){return n==o[0]}))==r(o)?e:[]}function v(n){if(\"TABLE\"==n.nodeName){for(var a=function(r){var e,o,u=[],a=[];return function n(r,e){e(r),t(r.childNodes,function(r){n(r,e)})}(n,function(n){\"TR\"==(o=n.nodeName)?(e=[],u.push(e),a.push(n)):\"TD\"!=o&&\"TH\"!=o||e.push(n)}),[u,a]}(),i=a[0],s=a[1],c=r(i),f=c>1&&r(i[0])<r(i[1])?1:0,v=f+1,p=i[f],h=r(p),l=[],g=[],N=[],m=v;m<c;++m){for(var T=0;T<h;++T){r(g)<h&&g.push([]);var C=i[m][T],L=C.textContent||C.innerText||\"\";g[T].push(L.trim())}N.push(m-v)}t(p,function(n,t){l[t]=0;var a=n.classList;a.add(\"tg-sort-header\"),n.addEventListener(\"click\",function(){var n=l[t];!function(){for(var n=0;n<h;++n){var r=p[n].classList;r.remove(\"tg-sort-asc\"),r.remove(\"tg-sort-desc\"),l[n]=0}}(),(n=1==n?-1:+!n)&&a.add(n>0?\"tg-sort-asc\":\"tg-sort-desc\"),l[t]=n;var i,f=g[t],m=function(r,t){return n*f[r].localeCompare(f[t])||n*(r-t)},T=function(n){var t=d(n);if(!r(t)){var u=o(n),a=o(n.map(e));t=d(n.map(function(n){return n.substring(u,r(n)-a)}))}return t}(f);(r(T)||r(T=r(u(i=f.map(Date.parse),isNaN))?[]:i))&&(m=function(r,t){var e=T[r],o=T[t],u=isNaN(e),a=isNaN(o);return u&&a?0:u?-n:a?n:e>o?n:e<o?-n:n*(r-t)});var C,L=N.slice();L.sort(m);for(var E=v;E<c;++E)(C=s[E].parentNode).removeChild(s[E]);for(E=v;E<c;++E)C.appendChild(s[v+L[E-v]])})})}}n.addEventListener(\"DOMContentLoaded\",function(){for(var t=n.getElementsByClassName(\"tg\"),e=0;e<r(t);++e)try{v(t[e])}catch(n){}})}(document)</script>"
#else
    os <<
    "<script charset=\"utf-8\">\n"
    "  var TGSort = window.TGSort || function(n) {\n"
    "    \"use strict\";\n"
    "    function r(n) { return n ? n.length : 0 }\n"
    "    function t(n, t, e, o = 0) { for (e = r(n); o < e; ++o) t(n[o], o) }\n"
    "    function e(n) { return n.split(\"\").reverse().join(\"\") }\n"
    "    function o(n) {\n"
    "      var e = n[0];\n"
    "      return t(n, function(n) {\n"
    "        for (; !n.startsWith(e);) e = e.substring(0, r(e) - 1)\n"
    "      }), r(e)\n"
    "    }\n"
    "    function u(n, r, e = []) {\n"
    "      return t(n, function(n) {\n"
    "        r(n) && e.push(n)\n"
    "      }), e\n"
    "    }\n"
    "    var a = parseFloat;\n"
    "    function i(n, r) {\n"
    "      return function(t) {\n"
    "        var e = \"\";\n"
    "        return t.replace(n, function(n, t, o) {\n"
    "          return e = t.replace(r, \"\") + \".\" + (o || \"\").substring(1)\n"
    "        }), a(e)\n"
    "      }\n"
    "    }\n"
    "    var s = i(/^(?:\\s*)([+-]?(?:\\d+)(?:,\\d{3})*)(\\.\\d*)?$/g, /,/g),\n"
    "      c = i(/^(?:\\s*)([+-]?(?:\\d+)(?:\\.\\d{3})*)(,\\d*)?$/g, /\\./g);\n"
    "    function f(n) {\n"
    "      var t = a(n);\n"
    "      return !isNaN(t) && r(\"\" + t) + 1 >= r(n) ? t : NaN\n"
    "    }\n"
    "    function d(n) {\n"
    "      var e = [],\n"
    "        o = n;\n"
    "      return t([f, s, c], function(u) {\n"
    "        var a = [],\n"
    "          i = [];\n"
    "        t(n, function(n, r) {\n"
    "          r = u(n), a.push(r), r || i.push(n)\n"
    "        }), r(i) < r(o) && (o = i, e = a)\n"
    "      }), r(u(o, function(n) {\n"
    "        return n == o[0]\n"
    "      })) == r(o) ? e : []\n"
    "    }\n"
    "    function v(n) {\n"
    "      if (\"TABLE\" == n.nodeName) {\n"
    "        for (var a = function(r) {\n"
    "            var e, o, u = [],\n"
    "              a = [];\n"
    "            return function n(r, e) {\n"
    "              e(r), t(r.childNodes, function(r) {\n"
    "                n(r, e)\n"
    "              })\n"
    "            }(n, function(n) {\n"
    "              \"TR\" == (o = n.nodeName) ? (e = [], u.push(e), a.push(n)) : \"TD\" != o && \"TH\" != o || e.push(n)\n"
    "            }), [u, a]\n"
    "          }(), i = a[0], s = a[1], c = r(i), f = c > 1 && r(i[0]) < r(i[1]) ? 1 : 0, v = f + 1, p = i[f], h = r(p), l = [], g = [], N = [], m = v; m < c; ++m) {\n"
    "          for (var T = 0; T < h; ++T) {\n"
    "            r(g) < h && g.push([]);\n"
    "            var C = i[m][T],\n"
    "              L = C.textContent || C.innerText || \"\";\n"
    "            g[T].push(L.trim())\n"
    "          }\n"
    "          N.push(m - v)\n"
    "        }\n"
    "        t(p, function(n, t) {\n"
    "          l[t] = 0;\n"
    "          var a = n.classList;\n"
    "          a.add(\"tg-sort-header\"), n.addEventListener(\"click\", function() {\n"
    "            var n = l[t];\n"
    "            ! function() {\n"
    "              for (var n = 0; n < h; ++n) {\n"
    "                var r = p[n].classList;\n"
    "                r.remove(\"tg-sort-asc\"), r.remove(\"tg-sort-desc\"), l[n] = 0\n"
    "              }\n"
    "            }(), (n = 1 == n ? -1 : +!n) && a.add(n > 0 ? \"tg-sort-asc\" : \"tg-sort-desc\"), l[t] = n;\n"
    "            var i, f = g[t],\n"
    "              m = function(r, t) {\n"
    "                return n * f[r].localeCompare(f[t]) || n * (r - t)\n"
    "              },\n"
    "              T = function(n) {\n"
    "                var t = d(n);\n"
    "                if (!r(t)) {\n"
    "                  var u = o(n),\n"
    "                    a = o(n.map(e));\n"
    "                  t = d(n.map(function(n) {\n"
    "                    return n.substring(u, r(n) - a)\n"
    "                  }))\n"
    "                }\n"
    "                return t\n"
    "              }(f);\n"
    "            (r(T) || r(T = r(u(i = f.map(Date.parse), isNaN)) ? [] : i)) && (m = function(r, t) {\n"
    "              var e = T[r],\n"
    "                o = T[t],\n"
    "                u = isNaN(e),\n"
    "                a = isNaN(o);\n"
    "              return u && a ? 0 : u ? -n : a ? n : e > o ? n : e < o ? -n : n * (r - t)\n"
    "            });\n"
    "            var C, L = N.slice();\n"
    "            L.sort(m);\n"
    "            for (var E = v; E < c; ++E)(C = s[E].parentNode).removeChild(s[E]);\n"
    "            for (E = v; E < c; ++E) C.appendChild(s[v + L[E - v]])\n"
    "          })\n"
    "        })\n"
    "      }\n"
    "    }\n"
    "    n.addEventListener(\"DOMContentLoaded\", function() {\n"
    "      for (var t = n.getElementsByClassName(\"tg\"), e = 0; e < r(t); ++e) try {\n"
    "        v(t[e])\n"
    "      } catch (n) {}\n"
    "    })\n"
    "  }(document)</script>";
#endif // 0
    ;
    return true;
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
    cout    << "..:: " << appname << " ::..   Version: ";
    print_version(cout, appversion);
    cout    << " Built: " << __DATE__ << "\n";
    cout    << "    Copyright (c) 2020-2022 Anton Savov\n\n";
    /*
    kicad BOM .xml:

    ---------------------
    export (version="D")
        design
            source
            date
            tool
            sheet
                title_block
                    title
                    company
                    rev
                    date
                    source
                    comment (number="1", value="")
        components
            comp (ref="U1")
                value
                footprint
                datasheet
                fields
                    field (name="Config")
                libsource (lib="Opamps", part="LM358", description="Blah, blah")
                sheetpath (names="/main/", tstamps="/5EB59912/")
                tstamp
        libparts
        libraries
        nets
    ---------------------
    new things in the format since v6:
    Note: it seems parts with "exclude_from_bom" are not written to the bom.xml at all
    ....
        components
            comp (ref="U1")
                ....
    +           property (name="Sheetname", value="")
    +           property (name="Sheetfile", value="something.kicad_sch")
    +           property (name="exclude_from_board")
    -       tstamp
    +       tstamps
    ....

    Note: a single component may have multiple such properties
    Note: "tstamp" seems to have been renamed to "tstamps" .. maybe its contents is different too
    ---------------------

    TODO: how do multi-unit symbols appear here?
    ---------------------
    perhaps all BOMs would have to be specified at once with a single invokation
    as_kicad_bom_mod bom1.xml 20 bom2.xml 100 bom5.xml 7
    where to spew out the result BOM? current dir? what filename?

    after the merging, it'd be nice to generate an extra field
    for each component that tells how many such there are from each input bom
    for example:
        C_0603#100n, 150, "ac4r.xml(100),w5k5.xml(50)"

    ---------------------
    when loading a BOM:
    - parse the components
    - apply any simplifications
    - find&replace stuff
    - set component count to 1 by default
    - sort (maybe by refdes:footprint:value)
    - any ordering? (in case you want certain types of components to be listed first, maybe all U stuff, then R)
    - merge similar stuff, sum the part count
    - put the bom filename into each component as a field
    */

    string ofn;
    string ofd;
    string odir; // optional subdir for individual BOMs
    vector<string> cfgfn;
    string refn;
    string exedir;
    // get the user config dir from the OS, with the app name appended
    fs::path usercfgdir;
    vector<string> v_cfg_lud; // config file lookup dirs
    if (argc >= 1)
    {
        // sloppy way to get the .exe path
        exedir = fs::path(argv[0]).parent_path().string();
        //cout << exedir << "\n"; return 0;
    }
    v_cfg_lud.push_back(exedir);
    v_cfg_lud.push_back(fs::current_path().string());
    {
        // get the standard config path, something like "/home/user/.config"
        std::vector<std::string> vvv = split_std_path(get_userconfig_path());
        if (vvv.size() >= 1)
        {
            // if it's just one path, append the program name, create that directory
            usercfgdir.assign(vvv[0]);
            usercfgdir /= appname;
            if (!fs::exists(usercfgdir))
            {
                cout << "* Creating config directory: " << usercfgdir.string() << "\n";
                fs::create_directories(usercfgdir);
            }
        }
        for (const std::string &s : vvv)
        {
            add_lookup_dir(v_cfg_lud, s, appname);
        }

        // get the standard path for data/resources, something like "/usr/share"
        vvv = split_std_path(get_data_path());
        for (const std::string &s : vvv)
        {
            add_lookup_dir(v_cfg_lud, s, appname);
        }
        //for (const std::string &s : v_cfg_lud) { std::cout << "--> " << s << '\n'; }
    }

    struct bc_t
    {
        // BOM + count
        string fn;
        int count;
        // maybe this could hold any additional arguments?
    };
    vector<bc_t>    v_bc;
    vector<string>  v_argz;
    bool arg_f_specified = false;
    bool do_gen_html = true;
    bool do_gen_csv = true;
    #ifdef FAKE_ARGZ
    #warning "FAKE ARGUMENTS ENABLED!"
    {
        v_argz =
        #if 0
        {
            #ifdef JJJ
            "-o", "wubwub", "-d", "plots/bom",
            "D:/antto/z_boms/nfc_reader_main_mk1.xml", "1"
            #else
            #endif // JJJ
        };
        #else
        {
            #ifdef JJJ
                #if 0
            "-d", "plots/bom",
            //"D:/antto/z_boms/nfc_reader_main_mk1.xml", "1"
            "D:/antto/z_boms/fakebom.xml"
                #else
            "-o", "wubwub",
            "D:/antto/z_boms/nfc_reader_main_mk1.xml", "5",
            "D:/antto/z_boms/xmega_w5k5.xml", "20",
            "D:/antto/z_boms/fakebom.xml", "10"
                #endif // 1
            #else
            "-o", "wubwub",
            "/home/h4x0riz3d/kicad_projects/same53_audio/same53_audio.xml", "5",
            "/home/h4x0riz3d/kicad_projects/x0xlarge/x0xlarge.xml", "20",
            "/home/h4x0riz3d/ZONE/pimpmykicadbom/fakebom.xml", "10"
            //"/home/h4x0riz3d/kicad_projects/jtag2swd2stdc14/jtag2swd2stdc14.xml", "10"
            #endif // JJJ
        };
        #endif // 0
    }
    #else
    {
        //int k = 1;
        //while (k < argc) { v_argz.push_back(argv[k]); ++k; }
        v_argz.assign(argv+1, argv+argc);
    }
    #endif // FAKE_ARGZ
    //print_vec(v_argz, "arguments");

    if (v_argz.empty())
    {
        cout    << "Error: no arguments supplied.\n\n";
        cout    << "  -c <cfg>          Config\n"
                << "  -R <fp2pkg>       Regex file for generating Package out of Footprint\n"
                << "  -d <path>         Sub-path for generating single BOM inside a project\n"
                << "  -o <filename>     Output filename for when merging multiple BOMs\n"
                << "  -Fhtml            Generate only .html\n"
                << "  -Fcsv             Generate only .csv\n\n"

                << "+---------------------------------------------\n"
                << "| Usage 1 (generate single BOM from eeschema):\n"
                << "|\n"
                << "|   " << appname << " [-d <subpath>] <bom1.xml> [count]\n"
                << "+---------------------------------------------\n\n"
                << "+-------------------------------\n"
                << "| Usage 2 (merge multiple BOMs):\n"
                << "|\n"
                << "|   " << appname << " -o <output_filename> <bom1.xml> <count1> <bom2.xml> <count2> [<bom3.xml> <count3>]\n"
                << "+-------------------------------\n";
        return 1;
    }

    // fiddle with the commandline arguments
    {
        string arg;
        size_t k = 0; //1;
        //bool flag_out = false;
        bc_t bc;
        enum
        {
            Z_NONE = 0,
            Z_OUT0,
            Z_IN0,
            Z_DIR0,
            Z_CFG0,
            Z_RE0,
        };
        int st = Z_NONE;
        while (k < v_argz.size()) //(k < argc)
        {
            arg = v_argz.at(k); //argv[k];

            switch (st)
            {
                case Z_NONE:
                {
                    if (str_compare(arg, "-o"))
                    {
                        if (ofn.empty()) { st = Z_OUT0; }
                        else
                        {
                            // meh, this is not entirely decent
                            cerr << "Error: multiple outputs specified.\n";
                            return 1;
                        }
                    }
                    else if (str_compare(arg, "-d"))
                    {
                        if (odir.empty()) { st = Z_DIR0; }
                        else
                        {
                            // meh, this is not entirely decent
                            cerr << "Error: multiple -d arguments specified.\n";
                            return 1;
                        }
                    }
                    else if (str_compare(arg, "-c"))
                    {
                        st = Z_CFG0;
                    }
                    else if (str_compare(arg, "-R"))
                    {
                        if (refn.empty()) { st = Z_RE0; }
                        else
                        {
                            // meh, this is not entirely decent
                            cerr << "Error: multiple -R arguments specified.\n";
                            return 1;
                        }
                    }
                    else if (str_compare(arg, "-Fhtml"))
                    {
                        if (!arg_f_specified) { do_gen_csv = false; }
                        do_gen_html = true;
                        arg_f_specified = true;
                    }
                    else if (str_compare(arg, "-Fcsv"))
                    {
                        if (!arg_f_specified) { do_gen_html = false; }
                        do_gen_csv = true;
                        arg_f_specified = true;
                    }
                    else
                    {
                        st = Z_IN0;
                        bc.fn = arg;
                        bc.count = 1; // default
                    }
                } break;

                case Z_IN0:
                {
                    // count
                    try
                    {
                        bc.count = std::stol(arg);
                    }
                    catch (const std::invalid_argument &ia)
                    {
                        cerr << "Error: invalid argument: " << ia.what() << "\n";
                        return 1;
                    }
                    v_bc.push_back(bc);
                    st = Z_NONE;
                } break;

                case Z_OUT0:
                {
                    ofn = arg;
                    st = Z_NONE;
                } break;

                case Z_DIR0:
                {
                    odir = arg;
                    st = Z_NONE;
                } break;

                case Z_CFG0:
                {
                    cfgfn.push_back(arg);
                    st = Z_NONE;
                } break;

                case Z_RE0:
                {
                    refn = arg;
                    st = Z_NONE;
                } break;

                default:
                    cout << "WTF!\n"; break;
            };
            ++k;
        }
        if (st == Z_OUT0)
        {
            cerr << "Error: -o switch requires an argument.\n";
            return 1;
        }
        if (st != Z_NONE)
        {
            if (st != Z_IN0)
            {
                cerr << "Error: invalid argument syntax.\n";
                return 1;
            }
            else
            {
                bc.count = 1;
                v_bc.push_back(bc);
            }
        }
    }
    // check the config files
    {
        // if -c or -R are specified, add their paths to the lookup dirs
        if (!cfgfn.empty()) { v_cfg_lud.push_back(fs::absolute(cfgfn[0]).parent_path().string()); }
        if (!refn.empty())  { v_cfg_lud.push_back(fs::absolute(refn).parent_path().string()); }
    }
    {
        if (cfgfn.empty())
        {
            // try to load one from a default location?
            cfgfn.push_back("pimpmykicadbom.xml");
        }
        {
            bool found = false;
            size_t j = 0;
            while (j < cfgfn.size())
            {
                found |= get_default_cfg_fn(cfgfn[j], v_cfg_lud, "config file");
                ++j;
            }
            if (!found) { return 1; }
        }
    }
    {
        if (refn.empty())
        {
            // try to load one from a default location?
            refn = "fp2pkg_regex.txt";
        }
        if (!get_default_cfg_fn(refn, v_cfg_lud, "regex file")) { return 1; }
    }
    // load the config files
    CFG cfg;
    {
        size_t j = 0;
        while (j < cfgfn.size())
        {
            cout << "* Reading config file " << cfgfn[j] << " ...\n";
            XMLDocument cfgxml;
            XMLError resxml = cfgxml.LoadFile(cfgfn[j].c_str());
            if (resxml != XML_SUCCESS)
            {
                cerr << "* XML error: " << cfgxml.ErrorIDToName(resxml) << "\n* Aborting...\n";
                return 1;
            }
            XMLElement *elem0 = cfgxml.FirstChildElement();
            if (elem0 == nullptr)
            {
                cerr << "* Error: couldn't get XML element.\n";
                return 1;
            }
            PARSER_RETCODE res = cfg.parse(elem0);
            if (res != RC_SUCCESS)
            {
                cerr << "* Parsing error.\n* Aborting...\n";
                return 1;
            }
            ++j;
        }

        // TESTING:
        // "(C|R)(_\\d*_\\d*Metric)_.+" with "$1$1"
        //cfg.v_fp2pkg_re.push_back("(C|R)(_\\d*_\\d*Metric)");
        {
            std::ifstream fre;
            fre.open(refn);
            if (fre.is_open())
            {
                cfg.parse_regex_file(fre);
            }
        }
    }
    // for the input files, accept project directories
    // since the xml is usually inside with the same name
    {
        size_t k = 0;
        while (k < v_bc.size())
        {
            fs::path tp = v_bc[k].fn;
            if ((fs::is_directory(tp)) || (tp.has_filename() == false))
            {
                tp = tp.parent_path();
                //cout << tp << "\n";
                tp /= tp.stem(); // *tp.end();
                tp += ".xml";
                //cout << tp << "\n";
                if (fs::exists(tp)) { v_bc[k].fn = tp.string(); }
            }
            ++k;
        }
    }
    // print stats
    {
        cout << "Input files:\n";
        size_t k = 0;
        while (k < v_bc.size())
        {
            if (!fs::exists(fs::path(v_bc[k].fn)))
            {
                cerr << "Error: input file \"" << v_bc[k].fn << "\" does not exist.\n";
                return 1;
            }
            cout << v_bc[k].count << "\tx " << v_bc[k].fn << "\n";
            ++k;
        }
        if (k == 0)
        {
            cerr << "Error: no input files specified.\n";
            return 1;
        }
        if (k == 1)
        {
            // single BOM export, the filename should be automatic here
            assert(ofn.empty() == true);
            // split the input file into basebath and filename
            // pimp-up the filename
            // insert the extra sub-path from the -d argument (if any)
            fs::path tmp = fs::absolute(v_bc[0].fn);
            fs::path fff = tmp.filename();
            fff.replace_extension(".html");
            tmp = tmp.parent_path();
            tmp /= odir;
            fs::path tmp2 = tmp;
            tmp /= fff;
            ofn = tmp.replace_extension("").string();
            // the subpath has to be created if it doesn't exist
            if (!odir.empty())
            {
                if (!fs::exists(tmp2))
                {
                    cout << "Creating directory: " << tmp2.string() << "\n";
                    fs::create_directories(tmp2);
                }
            }
        }
        // TODO: if single BOM - the filename can't be known yet, before the (first) BOM is processed
        else
        {
            cout    << "Output file:\n"
                    << ofn << "\n";
            fs::path ofn_html = fs::absolute(ofn);
            ofn_html.replace_extension(".html");
            cout << ofn_html << "\n";
        }
        //return 0;
    }

    // Note: CFG loading was here, moving it up

    // figure out based on the arguments what mode to run.. single BOM or merging multiple BOMs?
    cout << "---\n";
    {
        size_t k = 0;
        BOM_PARSER parser;
        while (k < v_bc.size())
        {
            // parse all input bom.xml files
            const bc_t &bc = v_bc[k];
            XMLDocument bomxml;
            XMLError resxml = bomxml.LoadFile(bc.fn.c_str());
            if (resxml != XML_SUCCESS)
            {
                // oops, some other error... halt
                cerr << "* XML error: " << bomxml.ErrorIDToName(resxml) << "\n* Aborting...\n";
                return 1;
            }
            XMLElement *elem0 = bomxml.FirstChildElement();
            if (elem0 == nullptr)
            {
                cerr << "* Error: couldn't get XML element.\n";
                return 1;
            }
            PARSER_RETCODE res = parser.parse(elem0);
            if (res != RC_SUCCESS)
            {
                cerr << "* Parsing error.\n* Aboring...\n";
                return 1;
            }
            ++k;
        }
        cout << "Parsed " << k << " BOMs..\n";

        // TODO: process each BOM
        k = 0;
        vector<BOM> &v_bom = parser.v_bom;
        while (k < v_bom.size())
        {
            BOM &bom = v_bom[k];
            bom.set_cfg(cfg); // should this be set even earlier than this?
            bom.process(v_bc[k].fn);
            bom.process2();
            bom.process3();
            ++k;
        }
        if (v_bom.size() == 1)
        {
            // single BOM export, get the auto-generated output filename
            BOM &bom = v_bom[0];
            if (!bom.aofn.empty())
            {
                //ofn = bom.aofn;
                // bom.aofn is just the filename, add the path from ofn
                fs::path p = ofn;
                p = p.parent_path();
                p /= bom.aofn;
                ofn = p.string();
            }
            cout << "Output file:\n";
            fs::path ofn_html = fs::absolute(ofn);
            ofn_html.replace_extension(".html");
            cout << '\t' << ofn_html << "\n";
        }

        // multiply each BOM by the required count
        k = 0;
        while (k < v_bom.size())
        {
            BOM &bom = v_bom[k];
            bom.mult(v_bc[k].count);
            ++k;
        }

        // individual sort
        for (BOM &bom : v_bom) { bom.sort(); }

        // merge the BOMs?
        BOM bom;
        BOM *thebom = &bom;
        bom.set_cfg(cfg);
        if (parser.v_bom.size() > 1)
        {
            cout << "Merging to a final BOM...\n";
            bom.merge_bom(parser.v_bom);
            bom.sort(); // hm..
        }
        else
        {
            // single BOM export, grouping hasn't been done yet?
            //bom = v_bom[0];
            thebom = &v_bom[0];
            thebom->group2();
            thebom->calc_stats();
        }

        // dumb, temporary
        if (!ofn.empty())
        {
            fs::path ofn_html = fs::absolute(ofn);
            ofn_html.replace_extension(".html");
            fs::path ofn_csv = fs::absolute(ofn);
            ofn_csv.replace_extension(".csv");
            ofstream os;
            if (do_gen_html)
            {
                cout << "Generating html...\n";
                os.open(ofn_html);
                if (os.is_open())
                {
                    //v_bom[0].gen_html(os);
                    thebom->gen_html(os);
                }
                else
                {
                    cerr << "Error: couldn't open output file (" << ofn_html << ").\n";
                }
                os.close();
            }
            if (do_gen_csv)
            {
                cout << "Generating csv...\n";
                os.open(ofn_csv);
                if (os.is_open())
                {
                    thebom->gen_csv(os);
                }
                else
                {
                    cerr << "Error: couldn't open output file (" << ofn_csv << ").\n";
                }
                os.close();
            }
        }
    }

    return 0;
}
