#include <cstdlib> // getenv()
#include <iostream>
#include "utils.hpp"

std::filesystem::path get_userconfig_path(const std::string appname)
{
    #ifdef _WIN32
    constexpr char what[] = "APPDATA";
    #else
    constexpr char what[] = "XDG_CONFIG_HOME";
    #endif //

    namespace fs = std::filesystem;
    fs::path p;
    const char* env_p = nullptr;
    if ((env_p = std::getenv(what)))
    {
        p.assign(env_p);
        if (!appname.empty())
        {
            // append this to the path
            p /= appname;
        }
    }
    return p;
}

std::filesystem::path get_data_path(const std::string appname)
{
    #ifdef _WIN32
    constexpr char what[] = "APPDATA";
    #else
    constexpr char what[] = "XDG_DATA_DIRS";
    #endif //

    namespace fs = std::filesystem;
    fs::path p;
    const char* env_p = nullptr;
    if ((env_p = std::getenv(what)))
    {
        p.assign(env_p);
        if (!appname.empty())
        {
            // append this to the path
            p /= appname;
        }
    }
    return p;
}
